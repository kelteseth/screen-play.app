---
title: "🎉 ScreenPlay v0.8.1 released! Bugfix for steam workshop path recognition. "
date: 2020-01-26T18:00:00+02:00
image: "images/blog/update_v081.png"
description: "🎉 ScreenPlay v0.8.1 released! Bugfix for steam workshop path recognition. "
author: "Elias Steurer | Kelteseth"
type: "post"
---

#### Our documentation received some love!
This is mostly a bugfix release with some minor quality improvements. 

#### Documentation Updates

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/72da276ccb2ac87cec3a8a7029bb9eea/image.png)

 * [Add better widgets documentation](https://kelteseth.gitlab.io/ScreenPlayDocs/widgets/widgets/)
 * Add example widgets
   * [CPU Widget ](https://kelteseth.gitlab.io/ScreenPlayDocs/widgets/example_CPU/)
   * [Storage Widget](https://kelteseth.gitlab.io/ScreenPlayDocs/widgets/example_Storage/)
   * [RSS Feed Widget](https://kelteseth.gitlab.io/ScreenPlayDocs/widgets/example_RSS/)

#### Fixes    
 * 1834898 Fix #48 button alignmentAdd button click able only if content is active
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/aef20001da9dc7911931c7bf785bc065/image.png)
 * 6c94ce5 Fix setting steam path
 * 5507f0a Fix #44Remove hardcoded langauge keys
     *  Remove currentIndex and ListModel comboBoxListModel
 * e35f6b7 Remove file:/// so the used does not get confused

#### Additions
 * 4c4f535 Add refresh ability of widgets via F5
 * d8a374e Add help text if storage path is empty
 * 38c6916 Add custom size, color and opacity for widgets to set
     * ![unknown](https://gitlab.com/kelteseth/ScreenPlay/uploads/0b4b7bf6d6dc51b83c9a06cf48d8816e/unknown.png)
 * 311cf42 Add changelog button
    * ![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/324793a0dbaaaae64c5c993622b89940/image.png)
