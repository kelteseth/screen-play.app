---
title: "🎉 ScreenPlay v0.12.1 released! Bundled FFMPEG And New Crash Reporter! "
date: 2020-09-18T18:00:00+02:00
image: "images/blog/update_v0121.png"
description: "🎉 ScreenPlay v0.12.1 released! Bundled FFMPEG And New Crash Reporter! "
author: "Elias Steurer | Kelteseth"
type: "post"
---

* Add sentry.io for better automatic crash reporting
* Remove ffmpeg downloader. It now comes bundled with ScreenPlay.
* Fix two crash on exit bugs.
* Fix sysinfo sdk not building shared bin
* Update SteamSDK to 1.50