---
title: "Celebrating 5 Years Of ScreenPlay  🎉"
date: 2022-03-25T12:00:00+02:00
image: "images/blog/5_years.png"
description: "ScreenPlay Is Now 5 Years Old!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

Today the *25.03.2022* marks the 5 year anniversary of the [first ever ScreenPlay commit](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b83116ff4f2f87e3e92e813eba6bded2fee9b894). Since then ScreenPlay has grown with the help of 10 contributors to more than 2.300 commits, 25k+ lines of QML/C++ code, 14 releases, thousands of daily active Steam users and a [growing collection of content!](https://steamcommunity.com/app/672870/workshop/) In the last 5 years ScreenPlay has seen a numerous amount of iterative changes.

<br>
<center>

![plausible](/images/blog/plausible_2022.png)

</center>

<br>

#### V0.15 - Qt6 with MP4(h.264) support and standalone installer without Steam
I'm currently working hard on the most requested features:
1. Qt6 is needed for leveraging the OS depending multimedia capabilities. This enables us to play mp4 files without converting them into VP8/VP9 first. This will enable better playback support, while lowering CPU and GPU usage in most cases.
2. Even though Steam is a very popular app for distributing and updating apps not everybody want to create a Steam account. For this we now create a installer for ScreenPlay. This installer is Windows only for now, because of osx signing issues.

<br>

#### Linux (KDE)
Sadly the linux version is not ready for prime time yet and has to be pushed to V0.16.

### Thank You!
To all people who have helped and supported this project 🎉

