---
title: "ScreenPlay v0.15 RC-1 Released!"
date: 2022-07-27T17:00:00+02:00
image: "images/blog/update_v015-rc1.png"
description: "ScreenPlay v0.15 RC-1 Released!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

### Qt6
Before we bore you with all the technical detail about the changes from Qt5 to Qt6, it is mostly about enabling some important features you can see below like Apple silicon and mp4 support. This also brings always a good amount of bugfixes.


### Playback Of H.264 (.mp4) Content
Qt6.2 ships with a completely revamped multimedia leveraging modern Windows, Linux and OSX APIs resulting in better performance and codec support. We now support h.264 on Windows and OSX. Even h.265 is supported when it is bought via the [Windows App store](https://apps.microsoft.com/store/detail/hevcvideoerweiterungen/9NMZLZ57R3T7?hl=de-de&gl=DE). Open Source codecs are always recommended to push Open Source software. 


<div class="container mt-5">
  <div class="row">
    <div class="col-sm">
      <h3>One Click Import And Export</h3>
      <p>
Sharing content just got a hole lot easier! You can simply now right click to export. This will export an <kbd>.screenplay</kbd> file, that contains all associated  project files. This uses 7zip internally to compress the data, that means it can also be opened with regular <a href="https://www.7-zip.org/">7zip</a>. Importing works also just by dropping a <kbd>.screenplay</kbd> filo onto ScreenPlay.
</p>
    </div>
    <div class="col-sm">
      <video autoplay muted class="container p-0 shadow-lg " controls="true" loop="true" allowfullscreen="true" >
    <source src="/videos/blog/import.mp4" type="video/mp4">
  </video>
    </div>
  </div>
</div>

<div class="container mt-5">
  <div class="row">
    <div class="col-sm">
    <img class="mw-100" src="/images/blog/lennart-uecker-EUlLDIRPO7c-unsplash(2).jpg" />
    </div>
    <div class="col-sm">
      <h3>Apple Silicon M1 and M2</h3>
      <p>Qt6 support brings us also free Apple Silicon support! This includes working Universal binary support, so now all ScreenPlay.app, ScreenPlayWallpaper.app, and ScreenPlayWidget.app will work with both Intel and Apple Silicon at full speed.</p>
  </div>
</div>


### Improved SysInfo API
The SysInfo API got a new IP-Address feature to list your public and private IP addresses. This will work for IPv4 and IPv6! For this we ping the free [icanhazip.com](icanhazip.com) service.


<br>
<p class="text-center">
  <img src="/images/blog/updates/ScreenPlay_V0.15-rc-1-sysinfo.png" />
</p>


### New Weather Forecast API
Another great addition to ScreenPlays expanding feature set is the new Weather API. It is build around [open-meteo.com](open-meteo.com), that will provide a detailed 7 day forecast. We now additionally include [Weather Icons](https://erikflowers.github.io/weather-icons/) for nice set of matching icons.


<br>
<p class="text-center">
  <img src="/images/blog/updates/ScreenPlay_V0.15-rc-1-weather.png" />
</p>

<div class="container mt-5">
  <div class="row">
    <div class="col-sm">
      <h3>Standalone Installer: None Steam (Workshop) Version</h3>
      <p>
ScreenPlay can now be independently  be downloaded. This includes a nice Qt IFW based installer. Note: This version does not include the Steam Workshop Plugin. You can still create Wallpaper via the Create tab or import any <kbd>.screenplay</kbd> file.

<a href="#chocolatey">Download Standalone Installer (Windows only)</a>

</p>
    </div>
    <div class="col-sm">
<img class="mw-100" src="/images/blog/updates/ScreenPlay_V0.15-rc-1-installer.png" />
    </div>
  </div>
</div>


### Revamped Window Navigation
ScreenPlay no longer uses the classic window navigation that caused some user experience issues, because the close button did not actually close ScreenPlay itself. The new control lets you minimize and exit ScreenPlay.



<br>
<p class="text-center">
  <img class="mw-100" src="/images/blog/updates/ScreenPlay_V0.15-rc-1-nav.png" />
</p>


### How get ScreenPlay v0.15 RC 1
The easiest way is to change  ScreenPlay -> Settings -> Betas -> Nightly. Standalone installer is Windows only for now.

<br>
<p class="text-center">
  <img class="mw-100" src="/images/blog/nightly.png" />
</p> 

### Chocolatey
Chocolatey is a free command-line package manager and installer for Windows. You can simply [install it with a single command](https://chocolatey.org/install#individual-method) from a Windows PowerShell prompt with administrator rights. Note: You have to specify __--pre__ because this is a pre release.

<br>

<code>
<kbd style="font-size:1.2rem">
choco install screenplay --pre 
</kbd>
</code>

### Direct download (Windows x64)

<br>

<div class="text-center mt-5">
  <a  href="https://kelteseth.com/releases/0.15.0-RC2/ScreenPlay-Installer.exe">
    <button type="button" class="btn btn-success"><span class="ti-download"></span> ScreenPlay-Installer.exe</button>
  </a>
  <a  href="https://kelteseth.com/releases/0.15.0-RC2/ScreenPlay-Installer.zip">
    <button type="button" class="btn btn-secondary"><span class="ti-download"></span> ScreenPlay-Installer.zip</button>
  </a>
  <a  href="https://kelteseth.com/releases/0.15.0-RC2/ScreenPlay-0.15.0-RC2-x64-windows-release.zip">
    <button type="button" class="btn btn-secondary"><span class="ti-download"></span> Portable ScreenPlay zip</button>
  </a>
</div>
<div class="text-center mt-1">
<a href="https://kelteseth.com/releases/0.15.0-RC2/ScreenPlay-0.15.0-RC2-x64-windows-release.zip.sha256.txt">Portable ScreenPlay zip sha256 checksum</a>
</div>
<div class="text-center mt-5">
  <a  href="https://forum.screen-play.app/t/screenplay-v0-15-0-rc-1/118">
      <button type="button" class="btn btn-primary " >
        <span class="ti-face-smile"></span>
        Join the pre-release discussion
      </button>
  </a>
</div>