---
title: "🎉 ScreenPlay v0.6 released! 4K Support and HTML5 Wallpaper! "
date: 2019-11-25T18:00:00+02:00
image: "images/blog/update_v06.png"
description: "🎉 ScreenPlay v0.6 released! 4K Support and HTML5 Wallpaper! "
author: "Elias Steurer | Kelteseth"
type: "post"
---

## Major Changes

* 🎉4K Monitor Support!

![4K Monitor Support](https://gitlab.com/kelteseth/ScreenPlay/uploads/8f1960a9ceaacb1e5b6aef40f7ccf155/image.png)

* 🎉HTML5 Wallpaper

![2019 by Grant Skinner (https://codepen.io/gskinner/pen/MpNjMO)](https://gitlab.com/kelteseth/ScreenPlay/uploads/51120c38b4eca73094ef35dba8f17e6e/1OHJ0upAPI.mp4)


* Qt WebEngine updated to be based on Chromium 77
* Better Steam support
   * Better detection of the Steam content Path
   * Fix when user never installed workshop content before




#### Qt 5.14 Related Changes 

*  9e0bece Bump the requirements to 5.14 for better high DPI scaling (and fixing window sizes on 4k monitors) and qmlRegisterSingletonInstance
*  18184d1 Fix  broken avatar width in Qt 5.14
*  92570a1 Fix silent startup inverted logicRemove center calculation casing buggy behavior on 4k Screens
*  0a27b61 Some refactoring of the wallpaper. Split video/webview into seperate qml file.Add sanity check for current time.
    *  Change  nimation from OpacityAnimation to states. This is a behavior change in Qt 5.14 resulting in not starting the animation when no mouse enter was detected at all.

### General Changes And Fixes

*  0ef3c46 Fix open project folder in windows explorer and formatting

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/494d1269c1fa2e632e56b2314e1e01e7/image.png)

*  bfc8619 Add more spacing fore single monitor in the sidebar when starting wallpaer for now
*  6913cf3 Fix starting of htmlWallpaper
*  a9c2949 Fix Html wallpaper 
*  47a81af Add nicer popup for ffmpeg download if the user declines
*  98a2f39 Workaround for type. Sometimes the time also contains the wallpaper executable path.
*  7258646 Cleanup project file using PWD paths
*  4d1a68b Add better directory discovery. Also create folder if use hasn't yet downloaded a workshop content.
*  bf70046 Add better check for Wallpaper app parameter
*  aca1f26 Fix active profile warning with wrong check when using __more_ thant one profile
*  72670e7 QtWebEngine initialize is not required if using AA_ShareOpenGLContexts
*  1456610 Simplify version check
*  1c9273f Fix logo and text
*  fa3d21d Move to VP8 which has way better encoding speed on integrated graphics than VP9 for now
*  1369e3f Add Qt 5.13 support to better investigate the 10 second delay on wallpaper creation
*  176742f Fix updating ffmpeg version string to use the correct version string now via a variable. This is like the most beginner error ever -.-
*  187d7ed Move more code from main to app class to have easier unit tests later.Fix activeProfilesTmp not reporing "We currently only support one profile!" when there is none.
    *  Move setupLanguage into separate function to have a slimmer constructor.
    *  Fix writing config where toJsonValue would write an nested wrong QJsonValues  and so aborting the insert method.
        *  newConfiginsert(name, value.toJsonValue());
        *  newConfig.insert(name, value.toString()); 
*  Fix checking steam path.
    *  Move util before every other class to get info logs during the other class constructors.
*  0a9452c Move redirect signals into header
*  30017db Remove topmargin after confirming that it was indeed never necessary. Nobody saw that!
*  c7d93b3 Replace {} with explicit std::nullopt
*  224b3d6 Add ThreeJSScene
*  4390e2d Fix language switching

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/2d6549917b3a1f8132fbd15295e2c319/image.png)

*  ab2704a Fix flickering of wallpaper creation. My guess is that I was wrong about windows 1903 changing the position of the wallpaper. I think it was manly because the with and height was set to late. Now we use the basewindow width an height property without the topmargin. When this code works on 21:9 we can remove the calc completely.
*  75b7b53 Fix missing icon
*  4ce1a87 Fix ScreenPlay content path
*  11610c2 Fix setting and updateting of the localStoragePath
*  ca022d7 Add GrowIconLink with links to my online profiles

![uXKEiKH0Kx](https://gitlab.com/kelteseth/ScreenPlay/uploads/84d55efd216d97670358335cc355a690/uXKEiKH0Kx.mp4)

*  17f6f81 Fix displaying of error messagesFix abort not being called
*  08414a3 Fix calling to load installed content twice
*  bb51212 Update FFMPEG to 4.2.1
*  c98ee8c Bump version to V0.5
*  ca42705 Add changelog link to gitlab in about