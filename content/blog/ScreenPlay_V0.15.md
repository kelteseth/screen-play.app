---
title: "ScreenPlay v0.15 Released!"
date: 2023-07-16T09:00:00+02:00
image: "images/blog/update_v015.png"
description: "ScreenPlay v0.15 Released!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

### Window Navigation Updates
ScreenPlay now adapts to the light and dark theme of the system. It also now features a new quick action menu on the top right for (left to right) muting, pausing, closing all wallpaper and wallpaper settings.

<br>
<p class="text-center">
  <img class="mw-100" src="/images/blog/updates/ScreenPlay_Window_theme.png" />
</p>



### Updated to latest Qt 6.5.1
Before we bore you with all the technical detail about the changes from Qt5 to Qt6, it is mostly about enabling some important features you can see below like Apple silicon and mp4 support. This also brings always a good amount of bugfixes.


### Playback Of H.264 (.mp4) Content
Qt6.2 ships with a completely revamped multimedia leveraging modern Windows, Linux and OSX APIs resulting in better performance and codec support. We now support h.264 on Windows and OSX. Even h.265 is supported when it is bought via the [Windows App store](https://apps.microsoft.com/store/detail/hevcvideoerweiterungen/9NMZLZ57R3T7?hl=de-de&gl=DE). Open Source codecs are always recommended to push Open Source software. 


<div class="container mt-5">
  <div class="row">
    <div class="col-sm">
      <h3>One Click Import And Export</h3>
      <p>
Sharing content just got a hole lot easier! You can simply now right click to export. This will export an <kbd>.screenplay</kbd> file, that contains all associated  project files. This uses 7zip internally to compress the data, that means it can also be opened with regular <a href="https://www.7-zip.org/">7zip</a>. Importing works also just by dropping a <kbd>.screenplay</kbd> filo onto ScreenPlay.
</p>
    </div>
    <div class="col-sm">
      <video autoplay muted class="container p-0 shadow-lg " controls="true" loop="true" allowfullscreen="true" >
    <source src="/videos/blog/import.mp4" type="video/mp4">
  </video>
    </div>
  </div>
</div>

<div class="container mt-5">
  <div class="row">
    <div class="col-sm">
    <img class="mw-100" src="/images/blog/lennart-uecker-EUlLDIRPO7c-unsplash(2).jpg" />
    </div>
    <div class="col-sm">
      <h3>Apple Silicon M1 and M2</h3>
      <p>Qt6 support brings us also free Apple Silicon support! This includes working Universal binary support, so now all ScreenPlay.app, ScreenPlayWallpaper.app, and ScreenPlayWidget.app will work with both Intel and Apple Silicon at full speed.</p>
  </div>
</div>


### Improved SysInfo API
The SysInfo API got a new IP-Address feature to list your public and private IP addresses. This will work for IPv4 and IPv6! For this we ping the free [icanhazip.com](icanhazip.com) service.


<br>
<p class="text-center">
  <img src="/images/blog/updates/ScreenPlay_V0.15-rc-1-sysinfo.png" />
</p>


### New Weather Forecast API
Another great addition to ScreenPlays expanding feature set is the new Weather API. It is build around [open-meteo.com](open-meteo.com), that will provide a detailed 7 day forecast. We now additionally include [Weather Icons](https://erikflowers.github.io/weather-icons/) for nice set of matching icons.


<br>
<p class="text-center">
  <img src="/images/blog/updates/ScreenPlay_V0.15-rc-1-weather.png" />
</p>

<div class="container mt-5">
  <div class="row">
    <div class="col-sm">
      <h3>Standalone Installer: None Steam (Workshop) Version</h3>
      <p>
ScreenPlay can now be independently  be downloaded. This includes a nice Qt IFW based installer. Note: This version does not include the Steam Workshop Plugin. You can still create Wallpaper via the Create tab or import any <kbd>.screenplay</kbd> file.

<a href="#direct-download-windows-x64">Download Standalone Installer (Windows only)</a>

</p>
    </div>
    <div class="col-sm">
<img class="mw-100" src="/images/blog/updates/ScreenPlay_V0.15-rc-1-installer.png" />
    </div>
  </div>
</div>




### All other features and fixes:
- Pack macos ScreenPlay into a single executable ScreenPlay.app
- Fix Windows fractional wallpaper scaling.
- Fix Windows wallpaper fade in calculation. We know correctly calculate the default Windows wallpaper position for nice fade-in animations.
- Add explicit quit and minimize warning dialog.
- Fix macos Wallpaper showing app icon in dock.
- Deprecate QtMaintanance based Qt setup. We now use aqt directly and thus no longer require a Qt account. If you are using VSCode you can simply install the [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) and [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) with the newly added `CMakePresets.json`. QtCreator still has some issues, see [my bugreport here](https://bugreports.qt.io/browse/QTCREATORBUG-28606).
- Add working linux CI. This also includes a working KDE Wallpaper version that is not yet 100% ready.
- Add first batch of [default content](https://gitlab.com/kelteseth/ScreenPlay/-/tree/master/Content) that will ship by default with the next ScreenPlay version.
- Update steamSDK to 1.56
- Fix widget and wallpaper live reloading
- Fix widget not propagating input
- Update to ffmpeg 6.0
- Change to shorter SPDX license in header files
- Add moc include for faster compile times
- Remove QApplication in favor of QGuiApplication to fix dark window border on Windows
- Add tasks.json for things like setup.py so it can be started from VSCode gui
- Refactor linux X11 support: Rename LinuxWindow to LinuxX11Window, for kde we use a desktop wallpaper plugin and for Wayland we need something else entirely.
- Add audio icon if installed wallpaper contains audio

<div class="text-center mt-5">
  <a  href="https://store.steampowered.com/app/672870/ScreenPlay/">
    <button type="button" class="btn btn-primary"><span class="ti-download"></span> Download ScreenPlay</button>
  </a>
</div>


### Direct (No Steam Workshop) download (Windows x64)
This is the standalone ScreenPlay version. If you wish to have Steam Workshop support with hundrets of wallpapers please download ScreenPlay from [Steam](https://store.steampowered.com/app/672870/ScreenPlay/).
<br>

<div class="text-center mt-5">
  <a  href="https://kelteseth.com/releases/0.15.0/ScreenPlay-Installer.exe">
    <button type="button" class="btn btn-success"><span class="ti-download"></span> ScreenPlay-Installer.exe (No Steam Workshop)</button>
  </a>
  <a  href="https://kelteseth.com/releases/0.15.0/ScreenPlay-Installer.zip">
    <button type="button" class="btn btn-secondary"><span class="ti-download"></span> ScreenPlay-Installer.zip (No Steam Workshop)</button>
  </a>
  <a  href="https://kelteseth.com/releases/0.15.0/ScreenPlay-0.15.0-x64-windows-release.zip">
    <button type="button" class="btn btn-secondary"><span class="ti-download"></span> Portable ScreenPlay zip (No Steam Workshop)</button>
  </a>
</div>
<div class="text-center mt-1">
<a href="https://kelteseth.com/releases/0.15.0/ScreenPlay-0.15.0-x64-windows-release.zip.sha256.txt">Portable ScreenPlay zip sha256 checksum</a>
</div>

