---
title: "ScreenPlay Video Wallpaper Guide: How To Create Video Wallpaper From Your Favorite Game"
date: 2021-05-07T12:00:00+02:00
image: "images/blog/guides/02VideoWallpaper.png"
description: "ScreenPlay Video Wallpaper Guide: How To Create Video Wallpaper From Your Favorite Game"
author: "Elias Steurer | Kelteseth"
type: "post"
---


Games allow us to dive deep into fantastic worlds. Sadly we cannot play all day long, but we can make some nice wallpaper to ease the pain!

<br>

#### What we will do in this guide:
1. Download the software we need for free 
2. Open OBS and adjust the settings 
3. Open your game and record a clip
4. Import, edit and export the created video in Davinci Resolve
5. Import the video into ScreenPlay

  <div class="p-4">
    <video  muted class="container p-0 shadow-lg" controls="true" allowfullscreen="true">
      <source src="/videos/blog/wallpaper_gothic_2.mp4" type="video/mp4">
    </video>
  </div>


<br>

#### 1.  Download the software we need. This software is free :
* Recording: [OSB Studio](https://obsproject.com/)
* Video Editing: [Davinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/)

An account is required for Davinci Resolve, but this takes less than 5 minutes to set up. For this press the "Download Now" button, register, download and install Davinci Resolve. You may have to restart your PC during the installation and then continue the set up after the restart.

![Download](/images/blog/guides/obs_danvinci_resolve.png)


### 2. Open OBS and adjust the settings
OBS is mainly known for Twitch streaming, but it also can be used for recording. When installed go to the settings "Video" tab. Change to your preferred resolution in both tabs for example 1920x1080 for 16:9 monitors.
![Download](/images/blog/guides/obs_settings.png)

Change the default recording format to mp4.
![Download](/images/blog/guides/obs_settings2.png)

If your sources are empty (right next to the scenes) click the "+" and add either a "Game Capture" to select a specific game window or you can simply record your whole desktop via "Display Capture".
![Download](/images/blog/guides/obs_settings3.png)


### 3. Open your game and start recording
Now you can record your game footage. For me, I started the best game ever made in Germany: Gothic 1 (and 2). Back then games still came with cheat codes, so I enabled god-mode and free camera movement.

![Download](/images/blog/guides/obs_capture.png)

### 4. Import and edit and export the created video in Davinci Resolve
Open Davinci Resolve and create a new project. Then drag and drop your video and audio files into the "Media Pool". From there you can drag the files into the timeline (white arrow). For simple cutting you can use the razor tool (orange box). The technique to create a seamless loop is as follows: Cut somewhere in the center of your clip the video in half. Now move the second clip at the beginning of the timeline and move the first clip directly at the end of the second clip. Now the clip at the end perfectly matches the clip at the beginning. We now have to bridge the two parts at the center together. For this we 
drag the second clip one layer up as seen in the image below. Then we open the "Effects Library" and drag and drop the "Cross Dissolve" effect onto the beginning of the second clip. With this we can now smoothly blend the beginning of the second clip over the end of the first clip. 

![Download](/images/blog/guides/davinci.png)

Feel free to add custom sounds or more effects. In my video I added a nice Vignette filter at the borders of the video. When you are all done click on the rocket ship at the very bottom. On the left side you can change the export settings. Set the Format to MP4 and leaf the other settings as is. Then hit "Add to Render Queue". Then at the top right press "Render".

### 5. Import the video into ScreenPlay
Now simply open ScreenPlay -> Create -> Import Video and Convert (All Types). Congratulations to your first wallpaper 🎉🎉 You can also upload your creations via the Steam Workshop to share your creations with others.

  <div class="p-4">
    <video  muted class="container p-0 shadow-lg" controls="true" allowfullscreen="true">
      <source src="/videos/blog/wallpaper_gothic.mp4" type="video/mp4">
    </video>
  </div>
<br>