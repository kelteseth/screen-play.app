---
title: "🎉 ScreenPlay v0.9 released! VP9 Video import support for better hardware acceleration on newer Nvidia and AMD cards! "
date: 2020-02-08T18:00:00+02:00
image: "images/blog/update_v09.png"
description: "🎉 ScreenPlay v0.9 released! VP9 Video import support for better hardware acceleration on newer Nvidia and AMD cards! "
author: "Elias Steurer | Kelteseth"
type: "post"
---

#### Major Changes

#####  🎉 VP9 import support. This can significantly increase your performance on newer NVidia hardware!
![vGLAX0cXCb](https://gitlab.com/kelteseth/ScreenPlay/uploads/61ff9c37b59b2ecee5464df86f8d5ac9/vGLAX0cXCb.mp4)

#####  🎉 HTML Walpaper wizard
![USP3FSWqZs](https://gitlab.com/kelteseth/ScreenPlay/uploads/a5c98971839a7a784205f62bd8cb5c23/USP3FSWqZs.mp4)

This release also fixes the detection of the Steam Workshop path for initial installations!

#### General
 * 16dbcab Bump version to 0.9
 * 9731b61 Formatting (1.000 commit :)
 * 6e39f0b Change background overlay strength
 * 8b38224 Change creating wallpaper popup. We  now properly close the save timer popup and switch the navigation to Installed

#### Additions
 * d50b4ad Add simple replacement of wallpaper
 * 734adf8 Add basic html wallpaper creation
 * 9c7bf62 Add mostly empty html wallpaper wizard
 * be5e247 Add dynamic width an height for wizards
 * 5ae555d Add help for choosing codec
 * 7a9769f Add enum for video codec selection
 * 8ea07ff Add multi step video import Wizard
 * cf9ae79 Add qdoc for CI
 * 9c49732 Add install-dependencies.sh for linux Move media files into .gitlab folder Change readme to better help with linux setup process

#### Fixes
 * 0e92a32 Fix casing of the ScreenPlaySDK
 * dddab50 Fix some linux compile issues
 * 22dba2b Fix CI ubuntu paths
 * ca131f7 Fix qdoc generation for windows
 * 17e3d91 Fix window minimizing when user presses minimize all windows. This apparently also fixes some overlay problems. This closes #53
 * 766edd5 Fix create button text overflow and add tooltip
 * a2cd405 Fix #45
 * 19b3bf9 Fix not clickable navigation by replacing it with the official quick controls navigation
 * e217c7f Fix html wallpaper using invalid paths
 * 5b8d4f5 Fix missing filepath when creating wallpaper
 * f72e54e Fix 1px window border
 * 6f3019b Fix workshop detectionAdd debug outputs for easier debugging
 * 36938b8 Fix wallpaper start
 * 9e85f2e Fix qmake variable overrides triggering wrong {}