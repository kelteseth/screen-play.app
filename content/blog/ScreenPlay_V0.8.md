---
title: "🎉 ScreenPlay v0.8 released! The Widget Release! "
date: 2020-01-19T18:00:00+02:00
image: "images/blog/update_v08.png"
description: "🎉 ScreenPlay v0.8 released! The Widget Release! "
author: "Elias Steurer | Kelteseth"
type: "post"
---

#### Major Changes

#####  🎉 Proper Widget support via QML or HTML code!
![tXCLjdYdvu](https://gitlab.com/kelteseth/ScreenPlay/uploads/48ba649444a0feee08242ecff6c79b5f/tXCLjdYdvu.mp4)

#####  🎉 Widget wizard that helps you create new widgets!
![v56t0Bv8tT](https://gitlab.com/kelteseth/ScreenPlay/uploads/b8c673aa4a782734e5103498e2181576/v56t0Bv8tT.mp4)

#####  🎉 Storage list model based of QStorageInfo for you to use via Sysinfo qml module
More info at: https://gitlab.com/kelteseth/ScreenPlay/blob/dev/ScreenPlaySysInfo/storage.h
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/1b02d6c4267c0b0d6835a14f2402ef5e/image.png)

#####  🎉 Full translations for (via deepl, can contain errors🤷‍♂️)
  * 🇪🇸 Spanish
  * 🇩🇪 German
  * 🇷🇺 Russian 
  * 🇫🇷 French
![awWMjujNmv](https://gitlab.com/kelteseth/ScreenPlay/uploads/52f843ec67d82d49cac8f739efcfb4ab/awWMjujNmv.gif)

#####  🎉 More developer documentation via qdocs (alpha) help needed!
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/78585fd429098fa4ed1b2d88259cc3de/image.png)

#### General Changes And Fixes

* 789abb1 Bump version to V0.8
* f1dd2ce Add decrease of widget counter when the user presses close in the widget
* bfd7a83 Fix install pathsAdd openSSL to readme as dep
* d110fbc Update openSSL to 1.1.1d via vcpkg now
* 20d2667 Add storage list model based on QStorageInfo
* a575331 Add translations for Spanish, French, German and Russian
* 369b01a Fix widget moving jitter thanks to bardao https://stackoverflow.com/a/55367441/12619313
* 1c3d89c Remove monitor and volume slider when a widget is selected.
* 63ada61 Add audio codec, audio file and video codec to the project.json. This is useful when the user want to search for a specific codec that would run better on his hardware.
* 0cfb951 Add application version to telemetry. This should help to find errors in development .
* 217d44c Disable the create button without functionallity for now
* 63a2cca Add nice fadeout on sdkDisconnected
* dbe1135 Fix sdk connection not sending appID with the "appID=" in the beginning of the string
* ad886d6 Add remove all widgets functionallity and ui button
* 7f59985 Add missing qrc entries for the example images
* 59890ec Fix prefferedWidth binding loop
* 7e2aeaa Add example image to the wizard for qml and html
* f72b416 Remove soon to be deprecated rootContext
* 939ee73 Fix crash when telemetry was disabled and still used to send events on an invalid pointer
* 04705a5 Add widget type to the ScreenPlayWidget executable as an argument. This can be ether qmlWidget or htmlWidget for now.
* 8640874 Add open content in explorerFix error when no preview image was selected. The preview image is optional but recommended.
* 14ee667 Add empty widget creator
* 0913dff Add base create emtpy qml/html widget
* 8085f0a Refactor wizards to more easily use wizards across more content like wallpaperFix default video wallpaper creation with the new size
* 9931f77 Refactor project wizard. Now we have ether import content or create new based on a template
* e4698df Add documentation for all classes and methodsAdd mermaid diagram
* 799b4a0 Fix loading of wizard elements
* 942f418 Add basic not working general wizard