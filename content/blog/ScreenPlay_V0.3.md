---
title: "🎉 ScreenPlay v0.3 released!"
date: 2019-08-28T18:00:00+02:00
image: "images/blog/update_v03.png"
description: "🎉 ScreenPlay v0.3 released!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

Add dependency download like ffmpeg and OpenSSL. Fix some animation and code refactoring.

###### Add
* Add FFMPEG binary download
* Add double click system tray icon
* Add widget fadeout
* Add wallpaper fadeout
* Add windows version detection for wallpaper fadeout
* Add ripple effect when creating wallpapers
* Add VCPKG to CI
* Add ImageSelector placeholder text 
* Add readme gif
###### Change
* Change ScreenPlayWindow to ScreenPlayWallpaper
* Change Qt smartpointer with std smart pointer

######  Update
* Update to SSL 1.1.1. We need from now on Qt 5.13 because of the upgrade of OpenSSL from 1 to 1.1.1

###### Fix
* Fix skip conversion on webm
* Fix windows 10 version >= 1903 topmargin. They changed the topmargin to 10% of the monitor height. Dunno why...
* Fix saving project files in utf8
* Fix sidebar toggle
* Fix windows 7 crash
* Fix create footer animation
* Fix namespace issues for qml enums
