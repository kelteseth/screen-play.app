---
title: "Learn the basics of QML for Wallpapers and Widgets in 5 minutes"
date: 2021-03-07T12:00:00+02:00
image: "images/blog/guides/01qmlonline_title.png"
description: "Learn the basics of QML"
author: "Elias Steurer | Kelteseth"
type: "post"
---

ScreenPlay uses the QML programming language for the Wallpapers, Widgets and the Interface itself. It is a mix of JSON, Javascript and CSS. 

### Simple box and text

 You can [click](https://tinyurl.com/ycfzndzh) on the image to edit the example live in your browser!

[![image](/images/blog/guides/01qmlonline.png)](https://tinyurl.com/ycfzndzh)


### Lets break down the example:
#### Item
Every QML file must contain exactly one root item (and some imports to get the functionality we need, but we just have to accept this for now). In this example we use an [QML Item](https://doc.qt.io/qt-5/qml-qtquick-item.html) as our root element. An `Item` is the base type of all QML components. You can think about it as an invisible rectangle that also has many more features like position, opacity, focus etc. and most important an `id` to be identified. `Id` must be unique in QML but are not mandatory.  

We use `anchors` to easily position a component in the scene without manual logic. The line `anchors.fill: parent` simply states to have the same `width` and `height` as the parent element. The parent element of the root here is our Window.

<br>

#### Rectangle
If you actually want a rectangle with a color you can use the type [QML Rectangle](https://doc.qt.io/qt-5/qml-qtquick-rectangle.html) for this. In this example we created a `Rectangle` as a child of our main Item. This `Rectangle` inherits all properties of the Item like `width`, `height`, `position`, `id` etc. We set the `width` to be calculated from the root items width multiplied by 0.5. We now can use the defined `id` root and access the property via the dot notation. In the next line we set the `height` to the same value as our `width` by simply binding the value. Bindings are a powerful feature, making writing even more complex interactions a breeze. The next two properties define the position relative to the parent element. The last property is the color of our Rectangle. This can be a color string like `"blue"`, `"orange"` or a hex value `"#333333"`. 

<br>

#### Text
A important QML component is the [QML Text](https://doc.qt.io/qt-5/qml-qtquick-text.html) type. As the name describes we use this to draw text. Text like Rectangle inherits all features from `Item`, so it also has the property anchors.  The line `anchors.centerIn: parent` is actually equal to the code below but way easier to read.

``` qml

    x: root.width / 2 - width / 2
    y: root.height  / 2 - height / 2

```

To actually set the text we simply can define a fixed string. We can add a value like the `width` of our orange rectangle to our string to show us the `width` as a number. For this we simply can add (`+`) it to our text. The QML engine keeps track of the values. This means if we resize the window it would also automatically change the text to the new width value. 
Neat!

<br>


### Adding the code to a wallpaper
Creating an empty QML wallpaper via the Create Wizards.
![image](/images/blog/guides/01create_qml_wallpaper.png)
Open the containing folder:
![image](/images/blog/guides/01open_folder.png)
Now you can simply copy and paste your code in the qml file. 

### What we learned:

- All QML component inherit (at least) from Item. This Item comes with many features like `anchors` & `id`
- QML Items have child/parent relationships similar to HTML
- QML uses `property bindings` to automatically update properties without manually writing code
- QML has dozens of components ranging from positioner, layouting, gifs, enbedded webviews and more
- It is easier to center in QML than in CSS

<br>

##### See also
- QML Guides: https://qmlbook.github.io/ch04-qmlstart/qmlstart.html#qml-syntax
- List of all QML types: https://doc.qt.io/qt-5/qtquick-qmlmodule.html#object-types