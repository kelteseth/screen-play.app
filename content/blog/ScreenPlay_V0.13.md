---
title: "🎉 ScreenPlay v0.13 released!  All New Wizards: Website Wallpaper, Gif Wallpaper, WebM Import, QML, HTML5, Website Wallpaper!"
date: 2021-01-24T18:00:00+02:00
image: "images/blog/update_v013.png"
description: "🎉 ScreenPlay v0.13 released!  All New Wizards: Website Wallpaper, Gif Wallpaper, WebM Import, QML, HTML5, Website Wallpaper! "
author: "Elias Steurer | Kelteseth"
type: "post"
---



###  [New Steam Early Access Trailer](http://www.youtube.com/watch?v=q-J2fTWDxw8)
[![](http://img.youtube.com/vi/q-J2fTWDxw8/0.jpg)](http://www.youtube.com/watch?v=q-J2fTWDxw8 "")

_This release contains 4 months worth of work with nearly 400 changes, fixes and features. Thank you for all the contributors and community members for their support!_

### Rewritten Create screen with more wizards:
#### WebM Wizard: No need for long conversion when the video is already a webm! This also means user can use external tool like the excellent open source project [handbreak](https://handbrake.fr/) for converting in bulk!

![ScreenPlay_tz07ovs3gv](https://gitlab.com/kelteseth/ScreenPlay/uploads/584d2fdb718220064470b6c6d8d763aa/ScreenPlay_tz07ovs3gv.png)

#### Gif Wizard: Import a gif as a Wallpaper!

![ScreenPlay_X9Kd09Rq5H](https://gitlab.com/kelteseth/ScreenPlay/uploads/f0f2ebf743dbdde01f04914ad6b1a5bb/ScreenPlay_X9Kd09Rq5H.png)
#### Even more Wiazards:
* Website Wizard: A simple link for a website to display as a Wallpaper.
* QMl Wallpaper: Create interactive QML! You can use the Krita QML exporter to add easy animations to your drawn art! https://twitter.com/Kelteseth/status/1334918795979264000
* HTML Wallpaper: Use the power of Chromium to create awsome content
* Dozens of bug fixes as well as (not yet ready) Linux and OSX improvements
* We now use Qt 6 for our developer documentation generation. This fixes a ton of uncreated function documentation.
* Code cleanup. We now use a stackview with fancy new transition that are also more efficient!
* Monitor (un)plug detection: Displays a warning and removes the wallpaper for now!


### Contributor
- [poly000](https://gitlab.com/poly000) Chinese translation

### General 
- [a8433d3](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a8433d3) Bump version to 0.13
- [489b00c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/489b00c) Enabled high dpi scaling that now works again with 5.15.2 only!
- [abc72f2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/abc72f2) Refactor item to use threaded animation via Animator
- [75ec4b0](https://gitlab.com/kelteseth/ScreenPlay/-/commit/75ec4b0) Replace Labs FolderDialog with Dialogs FileDialog
- [58362d8](https://gitlab.com/kelteseth/ScreenPlay/-/commit/58362d8) Replace depricate SkipEmptyParts enum
- [a16ad8a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a16ad8a) Add better startup animation and example text
- [a3da07f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a3da07f) Move footer to separate StartInfo
- [31a47fb](https://gitlab.com/kelteseth/ScreenPlay/-/commit/31a47fb) Fix right click menu opening multiple timesAdd qstr to text. Fix folder deletion
- [f6b02cf](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f6b02cf) Add steam plugin load only on WorkshopThis should make it possible again to open ScreenPlay without Steam again!
- [536d262](https://gitlab.com/kelteseth/ScreenPlay/-/commit/536d262) Add doctest and google benchmark
- [206203f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/206203f) Add auto exit when using --benchmarkMaybe we should exit after we created the ScreenPlay app. Depends on what kind of benchmark we use in the future.
- [79dc835](https://gitlab.com/kelteseth/ScreenPlay/-/commit/79dc835) Add links
- [f29a65e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f29a65e) Add shields
- [b169e60](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b169e60) Add missing emit
- [1a63646](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1a63646) Fix monitorlistmodel on monitor plugged in or outAdd Dialog when it happens for the user to setup again
- [c0f858c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c0f858c) Add basic windows service
- [2a659b1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2a659b1) Fix not initialized QJsonParseError
- [1fde330](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1fde330) Fix live wallpaper editing
- [6e89828](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6e89828) Add user notification that one has to reconfigure setupWhen a monitor gets pluggin in or out
- [2d8c9f2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2d8c9f2) Merge branch 'master' of https://gitlab.com/kelteseth/ScreenPlay
- [3b558e2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3b558e2) Fix loadFromFile
- [626455c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/626455c) Replace custom loader with swipeviewMove Dialogs into dedicated files
- [5a36e32](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5a36e32) Fix navigation item on open from hidden
- [5aa714a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5aa714a) Add new common types and WizardPageHeadlineSection for smaller headlines like h3
    - TextField that behaves like the Material design text field
    - WizardPage that implements start and exit signals as well as a simple one page loader in a ScrollView
    - Clean root id names to root everywhere
    - Add prefix for Common includes to not mix with QQC2
    - TextField name to avoid collision
- https://gitlab.com/kelteseth/ScreenPlay/-/commit/bbb827e Add toLocal to remove file:///
    - Add generic LicenseSelector & Add Licenes for CC, GPL, Apache etc.
    - Add generic FileSelector
    - Add required flag to TextField
- [ea367a7](https://gitlab.com/kelteseth/ScreenPlay/-/commit/ea367a7) Fix installed load on start
- [5d8a95e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5d8a95e) Fix fade in animation shadow
- [c20f837](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c20f837) Fix website Wallpaper. Add https:// to wizard.
- [4063f5f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4063f5f) Fix right click menu typeFix banner type. Must be var to be able to hold unsinged long long
- [c823c52](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c823c52) Fix enum names and casings
- [5a8583c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5a8583c) Fix steam workshopIDRefactor from int to QVariant that can hold unsinged long ling int that is needed because as of this writing steamid are now bigger than int
- [dd0c4b1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/dd0c4b1) Fix cmake vcpkg path
- [3b27096](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3b27096) Disable hover on scroll
- [c9dfd9a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c9dfd9a) Add positionViewAtBeginning when changing filter
- [4004b4c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4004b4c) Add allow_failure: true for now
- [4c2cb43](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4c2cb43) Update CI to Qt 5.15.2
- [0671e10](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0671e10) Disable AV1 import for now
- [82ead02](https://gitlab.com/kelteseth/ScreenPlay/-/commit/82ead02) Move StartInfo delegate into StartInfoLinkImageFix headlines lines Add drag and drop area
- [1085179](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1085179) Add ColorPick by Alberto Bignotti
- [1a73f98](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1a73f98) Fix wrong filename when using a filename with a dotRemove old labs fileselector
- [d1d933b](https://gitlab.com/kelteseth/ScreenPlay/-/commit/d1d933b) Decrease scale of preview because steam does not allow large previewsIn the docs it state 1mb, in the manual upload it states 2mb!?!
- [f49fa0d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f49fa0d) Fix missing m_length for webm import
- [74db71d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/74db71d) Fix incorrect navigation state and index
- [25a519c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/25a519c) Fix workshop sidebar styling
- [9bbe61e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9bbe61e) Fix default folder for localStoragePath
- [dd64e93](https://gitlab.com/kelteseth/ScreenPlay/-/commit/dd64e93) Fix UploadProject popup
- [daa76b8](https://gitlab.com/kelteseth/ScreenPlay/-/commit/daa76b8) Add Steam Workshop Agreement
- [cb34445](https://gitlab.com/kelteseth/ScreenPlay/-/commit/cb34445) Remove duplicate header
- [db40924](https://gitlab.com/kelteseth/ScreenPlay/-/commit/db40924) Fix  font capitalization
- [a8620f1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a8620f1) Fix qml and html preview image copy. Add errors emit to all returns
- [4c2b6db](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4c2b6db) Refactor welcome screenRemove no longer needed seets
- [ce1ea6f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/ce1ea6f) Add godot to the start info tools
- [7234edd](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7234edd) Merge branch 'Lucky-00-master-patch-10240' into 'master'Update README.md
- [33cb77e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/33cb77e) Fix small video detection
- [ccb1d50](https://gitlab.com/kelteseth/ScreenPlay/-/commit/ccb1d50) Fix webm importQML called it without the quality parameter. Lets make 50 default. This is not used in the webm import anyways...
- [ffc1ccf](https://gitlab.com/kelteseth/ScreenPlay/-/commit/ffc1ccf) Fix starting wallpaper that do not contain the properties json object
- [9defa00](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9defa00) Fix argument order for name and preview image
- [920a648](https://gitlab.com/kelteseth/ScreenPlay/-/commit/920a648) Fix type from QString to QUrl
- [a8bcac9](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a8bcac9) Fix wallpaper autostart and add CriticalError msgThis was caused because we have the wrong base path QDir workingDir(QDir::currentPath()); instead of QDir workingDir(QGuiApplication::applicationDirPath()); when we start the app from a different folder.

### Shader plugin
- [ddad721](https://gitlab.com/kelteseth/ScreenPlay/-/commit/ddad721) Add basic shader plugin
- [fe2eb81](https://gitlab.com/kelteseth/ScreenPlay/-/commit/fe2eb81) Add not working water shader
- [f6c6cf8](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f6c6cf8) Cleanup shadertoy shader and files
- [0fa5588](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0fa5588) Add shadertoy shadertoy

### Create Screen
![ScreenPlay_vbJ37AA0uz](https://gitlab.com/kelteseth/ScreenPlay/uploads/ec2009260f9a1a5d52039c60abc038ce/ScreenPlay_vbJ37AA0uz.png)
- [53f7b27](https://gitlab.com/kelteseth/ScreenPlay/-/commit/53f7b27) Add basic Gif and Website wallpaper
- [550982e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/550982e) Fix webm import
- [d153d3b](https://gitlab.com/kelteseth/ScreenPlay/-/commit/d153d3b) Add quality slider to import convert
- [462d7ac](https://gitlab.com/kelteseth/ScreenPlay/-/commit/462d7ac) Change ui to contain some tutorials
- [e27481f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/e27481f) Add basic new create screen
- [7e7b493](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7e7b493) Remove unused  Qt.labs.platform
- [f386d10](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f386d10) Restore video import functionallity Add not working AV1 support
- [4c7770b](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4c7770b) Add basic webm import
- [1eff5f7](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1eff5f7) Add more example content sidebar menu
- [3bcc835](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3bcc835) Add StartInfo useful links to projects and resources
- [494e227](https://gitlab.com/kelteseth/ScreenPlay/-/commit/494e227) Add startinfo content
- [02b25e1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/02b25e1) Add gimp entry, hover zoom and some cleanup
- [7302b2e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7302b2e) Refactor wizards into separate class & Add GifWallpaper
- [0fc1963](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0fc1963) Fix wallpaper custom margin
- [79af968](https://gitlab.com/kelteseth/ScreenPlay/-/commit/79af968) Add description and tldr legal linksChange to custom LicenseSelector in every wizard
- [f13b9b8](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f13b9b8) Add more wizards and wallpaper typesAdd WebsiteWallpaper
- [dd361ee](https://gitlab.com/kelteseth/ScreenPlay/-/commit/dd361ee) Change gif selection to clear on reset
- [393e48a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/393e48a) Change FileSelector to not directly expose combobox alias
- [a985666](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a985666) Sort create categories into Code and Video
- [760e916](https://gitlab.com/kelteseth/ScreenPlay/-/commit/760e916) Change default license to Creative CommonsIncrease min width
- [6d65791](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6d65791) Add gif and website wallpaper to Installed Type enumAlso document where to change code when changing this enum
- [6939ee2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6939ee2) Make all wizards consitentUse same parameters and functions to copy files and create folder
- [9a5602d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9a5602d) Change websiteWallpaper from source to url

### Community
- [5a8a81f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5a8a81f) Enable  CommunityNavItem steam workshop
- [af646ba](https://gitlab.com/kelteseth/ScreenPlay/-/commit/af646ba) Add asynchronous load to Community

![ScreenPlay_CbiIRIyczA](https://gitlab.com/kelteseth/ScreenPlay/uploads/e960f4ac9e6da99db1182dc5b9175af0/ScreenPlay_CbiIRIyczA.png)

### Welcome Screen

![ScreenPlay_RkzMowkvZh](https://gitlab.com/kelteseth/ScreenPlay/uploads/c8f31aec76b5d25958b890b7b5ae73dc/ScreenPlay_RkzMowkvZh.png)

### Building & CI
- [234300f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/234300f) Fix not existing folder
- [1a86a8c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1a86a8c) Fix ffmpeg download paths
- [ecc9377](https://gitlab.com/kelteseth/ScreenPlay/-/commit/ecc9377) Fix required dev setup
- [b5a5940](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b5a5940) Add CMake dynamic configure_file for every file in /Common/ffmpeg/
- [29a2856](https://gitlab.com/kelteseth/ScreenPlay/-/commit/29a2856) Update install_dependencies_windows.bat Now supports .7z Codex FFMpeg build extracting using 7-zip and msiexec.
- [b5dae42](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b5dae42) Add new ci build script and check stepThis is python based and is now functional for windows. We now should be able to extend this script for linux and mac. Move setup script to Tools folder.
- [72a5485](https://gitlab.com/kelteseth/ScreenPlay/-/commit/72a5485) Move vcpkg out of ScreenPlay source treeThis has some advatages. For one QtCreator starts way faster because it queries _ALL_ files in the source dir. This can take to minute long startup when using many packages. This makes some check python scripts easier to write.
- [a367b22](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a367b22) Fix build paths
- [3ad6a61](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3ad6a61) Fix build pathAdd check if build type arg is missing
- [2311cda](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2311cda) Format file with latest python 3.9 Apparently my python 3.6 install an old cmakelang
- [7e204d4](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7e204d4) Update docs
- [05f9bc0](https://gitlab.com/kelteseth/ScreenPlay/-/commit/05f9bc0) Update vcpkg to 26.12.20

### Docs

- [Overhauled developer documentation](https://kelteseth.gitlab.io/ScreenPlayDeveloperDocs/)
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/e13c3f9fd8136f02839197be82bd1985/image.png)

- [52d38e2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/52d38e2) Change qdoc version to Qt 6 because earlier version are unusable
- [86f9a75](https://gitlab.com/kelteseth/ScreenPlay/-/commit/86f9a75) Update Qt CI to  6.0.0
- [524bd10](https://gitlab.com/kelteseth/ScreenPlay/-/commit/524bd10) Refactor developer docs because qodc is now useable with qt6
- [aabaaaf](https://gitlab.com/kelteseth/ScreenPlay/-/commit/aabaaaf) Move Legal in Docs folder

### Translations
- [be1a17d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/be1a17d) Update ScreenPlay_zh-CN.ts
- [113e097](https://gitlab.com/kelteseth/ScreenPlay/-/commit/113e097) Update translations and add update script
- [0d90169](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0d90169) Add Chinese translations to UI
- [dc82a8d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/dc82a8d) update translations
- [997ec4f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/997ec4f) update translation
- [aaa4e5d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/aaa4e5d) Update HowToUpdateTranslations.md
- [b53fc68](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b53fc68) Update chinese language file

### Windows
- [3f4538e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3f4538e) Fix sentry native missing crashpad_handler

### Linux & OSX
- [6c7479f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6c7479f) Remove sentry for all OS except win
- [2ed7bf2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2ed7bf2) Remove sentry package for linux for now
- [3c314b5](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3c314b5) Add gold linker and add remove SysInfo for linux
- [64f527e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/64f527e) Fix invalid code that wont compile under gcc
- [612f37c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/612f37c) Add install_dependencies_linux_mac to CI
- [e1f75d6](https://gitlab.com/kelteseth/ScreenPlay/-/commit/e1f75d6) Add OSX
- [b43769c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b43769c) Fix osx ci not executing command in shell
- [f188e6e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f188e6e) Fix osx compilation and CI
- [2ca6773](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2ca6773) Fix build.py syntax
- [051a31a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/051a31a) Fix casing and macos build
- [2e4a937](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2e4a937) Fix missing snap
- [893eec7](https://gitlab.com/kelteseth/ScreenPlay/-/commit/893eec7) Fix linux ci compilation
- [bf1fa16](https://gitlab.com/kelteseth/ScreenPlay/-/commit/bf1fa16) Add basic kde support
- [da59b2f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/da59b2f) Add basic wallpaper logic to qml fileWe now use websocket for communcation. For this to work we must add the logic to the screenplaymanager class.
- [0153bee](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0153bee) Add not yet working WebSocketServer
- [d2080c2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/d2080c2) Add initialization to QJsonParseError
- [adfb82d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/adfb82d) Add missing WebSockets module
- [793b442](https://gitlab.com/kelteseth/ScreenPlay/-/commit/793b442) Fix Test.qml casing
- [06bc6b7](https://gitlab.com/kelteseth/ScreenPlay/-/commit/06bc6b7) add linux script in bash
- [3a32074](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3a32074) Add development info for KDE wallpaper
- [9fa317a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9fa317a) Fix osx build script