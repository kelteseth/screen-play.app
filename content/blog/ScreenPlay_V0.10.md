---
title: "🎉 ScreenPlay v0.10 released! Massive performance increase and wallpaper mouse interactions! "
date: 2020-03-14T18:00:00+02:00
image: "images/blog/update_v010.png"
description: "🎉 ScreenPlay v0.10 released! Massive performance increase and wallpaper mouse interactions! "
author: "Elias Steurer | Kelteseth"
type: "post"
---


### Major Changes

####  🎉 Massive performance increase when a wallpaper is not visible
![adOvId8VDV](https://gitlab.com/kelteseth/ScreenPlay/uploads/49ee78b692e38a2e1c87c218f71344e7/adOvId8VDV.mp4)
##### ScreenPlay does not render any video which results in 0% CPU and GPU usage!
![lgDw03pe2f](https://gitlab.com/kelteseth/ScreenPlay/uploads/48f1491f0dd97fa44135497bd1f152c6/lgDw03pe2f.mp4)
##### The audio will still continue to play as usual!
<br>

####  🎉 Forward mouse click events to wallpaper (HTML and QML)
![xfY7d7LqdE](https://gitlab.com/kelteseth/ScreenPlay/uploads/1c1377441b20a1c60b6513b955315ff5/xfY7d7LqdE.mp4)
<br>

####  🎉 Live reloading of wallpaper changes (HTML and QML)
![Kb5vw3ZnLf](https://gitlab.com/kelteseth/ScreenPlay/uploads/ad7b8f6a6fa3ad13231bb0fcb108608a/Kb5vw3ZnLf.mp4)
<br>

####  🎉 Support for more wallpaper settings types
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/120e34f7284edb790faf84212b2c8312/image.png)


#### 🎉 🇰🇷 Korean and 🇻🇳 Vietnamese language support provided by Deleted User f6a9k4y2o0u and naram.dash!
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/97b205e0f501927d7a0fbc46324d875e/image.png)

<br><br><br>
<div align="center"> 
<img src="https://about.gitlab.com/images/mvp_badge.png"  width="120" height="auto"><h5> This month's Most Valuable Person (MVP) is [Graphicscore](https://gitlab.com/Graphicscore) </h5> for adding support for a number of features like: Better property controls and helping with input key forwarding!</div >
<br><br><br>

### Bug reports
Please report any bugs in the forum. Discord is sadly not suitable for proper deep discussions!
* https://forum.screen-play.app/category/7/troubleshooting

### Help ScreenPlay
* Vote for ScreenPlay on [alternativeto.net]( https://alternativeto.net/software/screenplay/)
* You can contribute in many ways! You can find a handy guide [here!](https://kelteseth.gitlab.io/ScreenPlayDocs/contribute/contribute/)

<br>

# Complete Changelog:

### Added
 * Add Filesystemwatcher for wallpaper
    * This means we can now live edit wallpaper without reloading!
 * Add working videoFillMode as enum. The Sidebar now also uses this value as default!
##### Crash breakpad analytics
 * Add qt-breakpad to the README for git submodules
 * Add browser and buttons for click testing
 * Add breakpad support for ScreenPlayWallpaper
    * This generates a minidump file when ScreenPlay or ScreenPlayWallpaper crashes. With this file we can better analyze the cause of the crash!
 * Add google breakpad for crash analytics
##### Pause wallpaper rendering when wallpaper is behind a window
 * Add release event to mouse hook
 * Add working pause visual part of the video with settings
 * Add activeScreensList to base Window
* Add check for multiple monitors for pausing wallpaper if an app is maximized on the same screen
 * Add basic detection of fullscreen window. Only tested with one monitor and simply pause the wallpaper for now!
##### Language
 * Add Korean and Vietnamese languages to the feature list
 * Add Vietnamese language by Deleted User f6a9k4y2o0u
 * Add Korean language support with custom noto fontThis needed a larger refactoring because we only want to set the noto korean font when the korean font is selected. For this we use
 ScreenPlay.settings.font The font must be located inside the <workingDir>/assets/fonts/fontname.otf because it is to large for our resource .qrc file
* Add checkbox to monitor settings dialog
* Add delay for setting up mouse hook. This is a workaround for a lag on startup for now.
* Add support override of display text for properties
### Fixed
 * Fix accidental click of installed wallpaper by adding a MouseArea
 * Fix default value of language setting
 * Fix wallpaper not using mouse inputs when enabled
 * Fix get method if wallpaper creation in sidebar
* Fix Checkbox and Colorpicker in Monitor Settings
* Fix profile path for loading and saving settings
* Fix some styling issues
* Fix VisualsPaused check for multiple monitorsFix inverted bool logic
* Fix issue where not specifying a text field in wallpaper properties could lead to undefined behavior
 * Fix wallpaper window flags for now. 
This introduces a 1pixel border  but it is still the only way to reliably display the wallpaper without  interfering of the window manager. This means no longer minimizing when pressing "minimize all windows". Because of this the wallpaper is 2 pixel larger in height and widget and offset by one in x and y.
### Misc
* Refactor fullscreen window check to use the same check on one or more monitors
* Increase all combobox sizes to implicit 200 for now. So we do not cut text like qt 5.14 currently does
* Implement loadFromFile function for qml usage
* Reset log after 10000 character length
* Refactor settings to use enums instead of stringsRefactor settings to use QSettings instead of settings.json
* Remove Qt version checksAdd comments to the ScreenPlayWindow tests
* Remove old settings json