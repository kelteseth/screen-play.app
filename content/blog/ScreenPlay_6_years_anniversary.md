---
title: "Celebrating 6 Years Of ScreenPlay  🎉"
date: 2023-03-25T12:00:00+02:00
image: "images/blog/6_years.webp"
description: "ScreenPlay Is Now 6 Years Old!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

Today the *25.03.2023* marks the 6 year anniversary of the [first ever ScreenPlay commit](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b83116ff4f2f87e3e92e813eba6bded2fee9b894). Since then ScreenPlay has grown with the help of 10 contributors to more than 2.600 commits, 25k+ lines of QML/C++ code, 14 releases, thousands of daily active Steam users and a [growing collection of content!](https://steamcommunity.com/app/672870/workshop/) In the last 6 years ScreenPlay has seen a numerous amount of iterative changes. The next version of ScreenPlay (V0.15) is right around the corner with many (!) new features. You can already try it out on Steam when you enable the internal builds in the Steam app settings.

<br>
<center>

![plausible](/images/blog/plausible_2023.webp)

</center>

<br>

