---
title: "🎉 ScreenPlay v0.7 released! QtQuick3D Support for easy 3D Wallpaper integration and video import! "
date: 2019-12-31T18:00:00+02:00
image: "images/blog/update_v07.png"
description: "🎉 ScreenPlay v0.7 released! QtQuick3D Support for easy 3D Wallpaper integration and video import! "
author: "Elias Steurer | Kelteseth"
type: "post"
---

#### Major Changes

#####  🎉 QtQuick3D Support for easy 3D Wallpaper integration
<div>
<img width="100%" height="100%" src="/uploads/410fe806fc1d1c903bc05d840c388f95/image.png">
</div>

#####   🎉 Two pass encoding of Video Wallpaper. This means a slighter increase in time when converting a video, but a great increase in visual quality!

<div>
<img width="100%" height="100%" src="/uploads/9de7d41b555594c6c1f48326a4f788e6/image.png">
</div>

#### General Changes And Fixes
* 0a9857d Cache preview image
* 71da477 Fix muting of all wallpaper

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/51903f7da372b6abf9e4cd1bd42ca5fd/image.png)

* 5ee250c Fix progress of converting a video. Now the first 50% are for pass 1 and the last 50% are for pass 2 encoding.
<div>
<img width="100%" height="100%" src="/uploads/e12348d402e895c14758f2a0a0c75122/iUHq1IjNM9.gif">
</div>

* ef07c05 Refactor video creation. Now the code is more readableAdd to pass encoding for better visual quality
Add generic parseQByteArrayToQJsonObject static function to util class
* f0549a3 Add more telemetry for checking when a bug in creation occurred
* 5429a3a Add better statistic of what is actively used
* c7a33f3 Add filename as default name
<div>
<img width="100%" height="100%" src="/uploads/b0e9ec95aea69314d56616c6997444d3/image.png">
</div>

* 1094b09 Add smaller preview thumbnail generation. This should speed up loading preview images in the installed section. The image size is now the same as the width of the thumbnail. Also the image size gets reduced by tenfold. If no previewThumbnail is found the regular preview is used instead. This is not recommended.
* b8b39a2 Add qtquickcompiler to ci
* eb1b7b6 Change preview image size to a fixed width of 320. Also helps with loading many installed list items
* d01d329 Only load gifs on hover. This increases the loading speed when having many wallpaper installed
* 7621daa Remove no longer used Screen.qml
* 10a1611 Change counter set value to only change when we know the size of the finished list. This is to avoid weird sizing animation at the number in the nav item
* 9697cbc Change fade in animation
<div>
<img width="100%" height="100%" src="/uploads/4a0dc6d28689c449bddd4f2b3437375a/dKXGH4hRVf.gif">
</div>

* ba4c8ad Change fadein timer for faster animations
* c463e74 Add better video import quality
* b6f2750 Add failsave if settings are messed up. For this we will try to delete the old settings and then recreate default settings.
* 10e5b11 Remove no longer initial debug message for build hash. We now have the ui in the settings for this.
* a95e246 Fix critical error when saving values to json. All value were saved as a string. Even bool where converted to "true" or "false" as string
* b94f4a9 Fix fillmode combobox cliping
* 15a6ff0 Enable antonymous statistic
* 1e5ffd2 Enable autostart as default option
* ee85bc8 Make cover the default scaling to have no distortions
* bc8e05c Add missing emits to singals
* 61cffa6 Add a space in the toString method
* d99dbae Change search input to be smaller
* f7404cc Fix id to copy to clipboard
* 1aa8fda Fix CI windeployqt.exe using an non exsisting folder
* adbf846 Fix some import issues.Add copy to clipboard when an error occurred
Add ffmpeg and ffprobe arguments to error output
Add QStringList to string static helper function
* 6c88fc8 Add spanish, russian and frensh translation files without translation
<div>
<img width="100%" height="100%" src="/uploads/bafa131889e2e87ff20f1c90dd010f92/image.png">
</div>

* e22d756 Remove antialiasing properties
* 83018ad Fix search look and feel
* 1dfd6c7 Fix Blog url
* 350a0dd Bump stomt sdk version to fix install step
* 41c7b74 Add Docs trigger to build the docs on every commit at https://gitlab.com/kelteseth/ScreenPlayDeveloperDocs
* 5104539 Fix ScreenPlay Dev Docs title
* 6ea2c6b Update Docs/config.qdocconf
* 85a99ae Fix Qt path Add mermaid support
* 277ff32 Fix documentation name to include generated
* 887ec4e Remove old docs folder
* 6dfb129 Add disabling of anonymous telemetry via settings
* e0b1a6c Add anonymous telemtry. This will only used for basic statisitcs like how many people have screenplay open and which menu point is clicked the most.
* 31fe7a9 Add basic telemetry
* f018964 Fix install dependencies
* a8aa4ed Update README.md
* f8c4eeb Remove vcpkg submodule. Now installable via vcpkg-install-dependencies
* fd94d65 Cleanup third party libs into common folderRemove old OpenSSL which now get bundled with Qt installer
* 0d9eced Add nlohmann-json
* 1259cbb Bump version to 0.6
* d76773f Add copy to clipboard util and feature to expander to easily copy logs
