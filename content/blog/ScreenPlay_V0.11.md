---
title: "🎉 ScreenPlay v0.11 released! Dark Mode, cmake, OSX, big cleanup and many fixes! "
date: 2020-06-06T18:00:00+02:00
image: "images/blog/update_v011.png"
description: "🎉 ScreenPlay v0.11 released! Dark Mode, cmake, OSX, big cleanup and many fixes! "
author: "Elias Steurer | Kelteseth"
type: "post"
---


### 🎉 Dark Mode
* ScreenPlay dark mode has been implemented. For most of the static color variables have been replaced by theme based ones. You can switch between a white and a dark theme in the settings. You do not need to restart the app for this. System based detection is not implemented yet.

![2020-06-07_13-50-15](https://gitlab.com/kelteseth/ScreenPlay/uploads/0aa2a0d0aba0e470cb3e685e28c1a6cd/2020-06-07_13-50-15.mp4)

### 🎉 Moving from qmake to cmake (how ScreenPlay is build/compiled)
* Moving from qmake to cmake was a _big_ effort. I had to change all pre and post build logic to modern cmake and also port all dependencies to cmake. In the end the cmake version is more readable and a _lot_ faster to compile! This step was do to Qt6 deprecating the old qmake and porting everything to cmake.
* ScreenPlay is now compile everything into one directory. This allows to remove some code that was needed before for detecting the path for the ScreenPlayWallpaper and ScreenPlayWidget executable, because build folder structure  was different on Windows, Linux and MacOS with qmake.
* Port the Stomt-QML (Apache 2.0) plugin to cmake and moved the repo to [gitlab](https://gitlab.com/kelteseth/stomt-qml)

### 🎉 Better Wallpaper-Mouse interactions
Wallpaper now receive more inputs like current mouse position without the need to press a mouse button.

### 🎉 Rewrite how to save wallpaper
The old logic for how to save wallpaper had many flaws and has been mostly rewritten. This allows us now to save multiple wallpaper and paves the way for saving wallpaper settings (like audio in video wallpaper and custom properties in qml wallpaper) in the next update. 

### 🎉 Explicit closing of wallpaper
Every wallpaper gets now a explicit quit command before closing the QLocalSocket connection. This was due to a strange behavior with windows pipes where they do not close the connection sometimes resulting in still active wallpaper.

### 🎉 Check if another ScreenPlay instance is running
Many user reported that they opened ScreenPlay many times by accident. This logic has been changed so that ScreenPlay now checks if another instance is running and closes itself if it finds one. The other ScreenPlay instance thens shows the window.

![2020-06-07_13-46-51](https://gitlab.com/kelteseth/ScreenPlay/uploads/b6b67f5f92542d2e761e99dee2f4b6ed/2020-06-07_13-46-51.mp4)

### 🎉 No more Windows media controls
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/0e462975cce0575cb2c8e5c75f020a45/image.png)

### 🎉 OSX port
OSX now has a working port but not all features have been tested. If you find any bugs please report them on GitLab or on our Discord.

### 🎉 Code Cleanup
ScreenPlay is now compiled with the latest Qt 5.15 (older version should still work). Qt 6 (the next major version) deprecation warnings have been fixed. 
* Wallpaper and Widgets now have their own enum type (c++ and qml) instead of strings 
* Fix documentation style and add many small documentation texts
* The entry point for the ui (main.qml) has been renamed to Wallpaper.qml and Widgets.qml for Wallpaper and Widgets
* Change QML slots style to Qt 6
``` qml
    // Old syntax 
    onRequestNavigation: {}
    // Nex syntax with function keyword and function arguments
    function onRequestNavigation(nav) {}
```

### All commits
* c3de439 Remove high DPI scaling for now
* 49e5b5f Fix telemetry when to send errorsRemove unused constructor
* 9b9697f Add missing getter from last commit
* fb503d2 Cleanup ProjectSettingsListModelRemove unnecessary shared_ptr. Fix dark mode white background by moving from Rectangle to Item
* b035ec7 Fix list model using wrong comparison
* 9410375 Fix more casing errors of wallpaper type
* 4cf2318 Change QSysInfo to be private memberFix include order
* 53f618a Fix duplicate call of close function
* ae6d697 Remove space
* b99c149 Fix wrong c++ function name in qml
* a817b34 Bump version to 0.11.0
* 6581736 Fix wrong c++ function name  in qml
* 43666b0 Add explicit quit command when removing a wallpaper
* d67ccb5 Fix casing error when closing wallpaper
* 4a70963 Fix buggy mouse click input by using the same position and mouse flags on release
* a5ef63e Rename main qml file of wallpaper and widget to match their project names
* 23a2e5e Add mouse movement on wallpaper without click
* 23209f4 Merge branch 'master' of https://gitlab.com/kelteseth/ScreenPlay
* feaa870 Fix issue #84
* d4ff126 Fix missing import to recognize c++ enums Language and Theme
* 4f175e6 Rename setWallpaperValue to setWallpaperValueAtMonitorIndex to be clearer about the indexsame with requestProjectSettingsAtMonitorIndex
* f69c4a1 Fix colors
* a57a475 Remove old stomt sdk
* 722632f Remove json loading code that via Util::openJsonFileToObject(file)
* f4b7cd8 Change rowCount to use a separate variable for easier reading
* 1d692fa Remove not used member
* 8909a30 Change Monitors list model to use c++ enums instead of strings for wallpaper type
* 6b6b2da Fix preview image by adding absolute path
* 8fc5b2a Change sdkconnector->closeWallpaper to return success  via bool
* d8b3814 Fix missing license
* 2ac4540 Add AGPL and Commercial copyright header
* 6900bcc Add single shot test if connected after 1 secondThere can be a rare case where disconnected is never fired because it was never connected in the first place. For this we wait 1 second and check the state. If not connected we abort.
* 2df1d75 Change profile settings to be able to save n amount of wallpapers
* 6148288 Change name to ${PROJECT_NAME}
* 191dc7c Fix ScreenPlay cmake path
* 9ce9467 Fix cmake be compilable on linuxDowngrade to 3.16 that ships with Ubuntu 20.04
* 52fb670 Change widget and wallpaper types to use enums Apparently this is the only way for qml to work
* c7106a8 Merge branch '37-support-system-wide-dark-mode' into 'master'Resolve "Support system wide dark mode"
* 8367303 Fix dark/light styling
* 8da12bb Fix more theming stuff for light and dark mode
* 23e9363 Content of patch-1 https://gitlab.com/PoorPocketsMcNewHold/ScreenPlay/-/commit/6190655d231d9efcf5e3db92ba4c8f90ce67309a — 
* ef74e7a  Creation of the ¨Import a Creation¨ and ¨Upload a Creation¨. — Correction of the ¨Wallaper¨ typo.
* 7adfe5c  Correct translation of ¨Create Widgets and Scenes¨ as it was translated in the wrong language
* 6d5aae8 Removal of the mention of Windows, as Screenplay is multi-platform.
* b868be7 Fix initialization order
* d4812e4 Change many dark and light theming issues
* 49a33fa Change Connections syntax to 5.15 style
* f3f3f8b Remove Qt Windows HighDPI Scaling because of buggy monitor width and height values QTBUG-81694
* 37d021c Add missing index.html for the wallpaper to cmake install step
* 49aad4b Update breakpad cmake integration
* 6190655 Merge branch '66-port-screenplay-from-qmake-to-cmake' into 'master'Resolve "Port ScreenPlay from qmake to cmake"
* 799a115 Add cmake QtCreator readme setup
* b69d715 Add copy of files into the qt/qml folder
* fba846a Add copy of korean fonts
* bd77099 Remove old project files
* 1cca1d0 Update vcpkg install method
* a944582 Fix stomt path
* e29571a Fix pathing because we now compile everyting into the same build dir
* c7fd69a Add first working cmake version for ScreenPlayRefactor stomt sdk from qmake/github to cmake/gitlab
* 5d7a97f Remove old stomt qt sdk
* ee133b8 Fix macos start of wallpaper on release for now. This will not work in the steam version because the apps in the steam version are in the same folder
* 0575c30 Fix opening folder on macos
* 27d298e Merge branch 'master' into 66-port-screenplay-from-qmake-to-cmake
* 36644ba Fix wallpaper functionallity for one screen on MacOS
* 1365cfc Fix ffmpeg download for macos not using mac files
* a837bd3 Fixed macOS compilation by replacing std::optional obj.value() that throws with *obj
* 3a8c686 Remove linux code files from macos
* 61aa1d5 Add MacOS version of vcpkg dependency installation
* fdce06e Fix old copy of files we no longer need because we use static libs for now
* Removing QtBreakpad for now for linux and mac
* 6fdf2f5 Fix mouse hook not being called on HTMLWallpaper
* ed450a4 Change all std smart pointer to use std:: instead of using "using" to be more consistent
* dc86de9 Add basic implementation of new profile system
* 8a3ec76 Add reload animation when reopening the main window by using the switchPage loader
* ef06a08 Change scrolling behavior to be less abruptly
* f9d2c9c Fix documentation style by using \brief everywhereChange Qt version to 5.15 for better qdoc analysis
* Remove no longer used fixWindowsPath function
* d19fe5b Merge branch '60-check-if-other-instances-of-screenplay-are-currently-running' into 'master'Resolve "Check if other instances of ScreenPlay are currently running"
* 68871cd Fix quit of QGuiApplication because the main event loop (app.exec) wasnt invoked yet
* a021a0b Add first version for detecting if another instance of ScreenPlay is running
* 71cd225 Bump version to 0.10.1
* fd3a177 Update Russian translations based thanks to user Observer
* 7e90f63 Change testing path to something download able from the steam workshop
* cab12c4 Change wallpaper check again because the old code didn't work for WS_MAXIMIZE windows that weren't in the foreground
* 2568abb Fix import of very small videos like 1 second in length. Thanks user Jailbreaker for the feedback!
* 99fc40a Change name filter for video import. For this wizard we do not support gif
* 5d32ff8 Fix visible check for one monitor
* fb782e5 Add small pause to wait until window animation is finished to not display black background for a few frames
* 4a6c46f Fix default cover mode
* 433c7f7 Fix localserver not stopping correctly on exit
* 17c2f60 WIP: Desktop Overlay rendering
* d414a9b Fix wallpaper and widget binary paths for linux debug build
* 6079da1 Fix project.json path not converting to localfile properly
* 153c871 Fixi profile.json unable to create on linux
* 2df2df1 Migrate from QGuiApplication to QApplication for linux support
* 0af6940 Add breakpad.pri for all platforms except linux for now
* a0bfeb0 Fix missing linker flags for release
* b9d8a8b Fix last missing linker flag! ScreenPlay is now once again compilable on linux!
* 49cabc3 Fix more linux linking errors
* d066ac2 Fix linux compilation
* 2c760fe I did some translation :p
* 85b897a Remove CONTRIBUTING see https://kelteseth.gitlab.io/ScreenPlayDocs/contribute/contribute/
* 39e81bb Add warning if removeWallpaperAt was not successful
* db94ca2 Fix clang compilation on windows by making m_globalVariables no longer a referenceFix clang-tidy and clazy warnings
* 0658d3f Fix 5.15 warnings for c++ singals to QML slots not using function
* 1cc2ec3 Fix default header value for checkWallpaperVisible
* cfbfa33 Remove CheckWallpaperVisible for now from default
* b4289fb Fix copy to clipboard for debug messages not using newline resulting in one line pasting
* 3b5ee22 Add description text when the wallpaper does not render to guide the user to disable the VisualsPaused feature if he can read this
* 527423c Fix theme set on startup
* bbc7c7c Change more QML Items from hardcoded values to Material variables
* bdc4b4c Merge branch 'dev' into 37-support-system-wide-dark-mode
* 422c4ef Add warning message for a potential bug where the wallpaper counter never resets
* 5d0fcd8 Remove none exisitng property
* 8b347ca Fix version an add auto install script to replace install-dependencies
* 6012839 Fix MonitorSelection colors
* cc59ef4 Fix more wrong colors
* 1aed38d Add basic dark/light/System switch
* 2e20673 Fix cmake files to be able to open the project in QtCreator
* d1bee5a Add autoconversion based of https://github.com/davidtazy/QMake2CMake