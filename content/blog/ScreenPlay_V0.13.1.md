---
title: "ScreenPlay v0.13.1 Released! Windows Monitor Scaling Support, Steam Tags Searches And Paging Support!"
date: 2021-05-02T14:30:00+02:00
image: "images/blog/update_v0131.png"
description: "ScreenPlay v0.13.1 Released! Windows Monitor Scaling Support, Steam Tags Searches And Paging Support!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

This release contains 4 month worth of updates and features!

<br>

#### Website
- New Blog Post: [Learn the basics of QML for Wallpapers and Widgets in 5 minutes](https://screen-play.app/blog/guide_learn_the_basics_of_qml/)
- Cleanup old blog posts and add custom image.
- [Many articles have been updated in our wiki!](https://kelteseth.gitlab.io/ScreenPlayDocs/)

<br>

### V0.13.1 Patchnotes:

- Fixed: ScreenPlay does not handle Windows display scaling for High-DPI screens like 4k [issue 125](https://gitlab.com/kelteseth/ScreenPlay/-/issues/125)


  {{< tweet user="Kelteseth" id="1380901091047510021" >}}

- Fixed: The second wallpaper does not close properly [issue 123](https://gitlab.com/kelteseth/ScreenPlay/-/issues/123)
- Add logging to sentry This will only get send on crash to help to understand the context of the crash
- Fixed: Some mouse forwarding
  {{< tweet user="Kelteseth" id="1380189221609218051" >}}

<br>

#### Community
Add newsfeed from https://screen-play.app/blog/
  <div class="p-4">
    <video  muted class="container p-0 shadow-lg" controls="true" allowfullscreen="true">
      <source src="/videos/blog/update_131_community.mp4" type="video/mp4">
    </video>
  </div>
<br>

#### Workshop
- Refactor search to be selectable and add icons
- Add mouse WaitCursor indicator on load

![image](/images/blog/update_v0131_workshopSearch.png)
#### Add paging support
This is planned to change in the future into an endless scrolling experience.
![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/d54a9690141214a3ff6a1548e157cfcb/image.png)

<br>

#### Add tag and tag search
  <div class="p-4">
    <video  muted class="container p-0 shadow-lg" controls="true" allowfullscreen="true">
      <source src="/videos/blog/update_131_tags.mp4" type="video/mp4">
    </video>
  </div>

- Fix background blur animation and values
- Refactor sidebar to better include tags. These tags are now clickable to be used for quick search!
- Refactor Steam plugin to unload when switching back to and other page like "Installed". Steam is not required to be running ScreenPlay!
- Fix preview image missing if workshop item has no gif preview
- Replace custom TextField search with regular TextField

<br>

#### Wallpaper
- Fix set replace wallpaper from type video to gif
- Fix settings wallpaper value

<br>

#### Installed
- Add sort by date for installed content
- Add titile if no preview image or video is available
  <div class="p-4">
    <video  muted class="container p-0 shadow-lg" controls="true" allowfullscreen="true">
      <source src="/videos/blog/update_131_sorting.mp4" type="video/mp4">
    </video>
  </div>
- Fix invisible Navigation height blocking imput
<br>


#### Language
- Add Portuguese (Brazil) translation. Thanks to Douglas Vianna @dgsmiley18 🐱‍🏍
#### Sysinfo
- Add uptime to sysinfo. You now can use the Sysinfo QML plugin to display how long 

  {{< tweet user="Kelteseth" id="1359884819535187978" >}}

#### MacOS
- Most work is currently happening here: https://gitlab.com/kelteseth/ScreenPlay/-/merge_requests/56
- Fix macos compilation
- Fix macos dynlib
#### Code cleanup
- Move used code into own ScreenPlayUtil library to be used elsewhere like project parsing and content type to/from string
- Move projectfile to be used elsewhere
#### Docs
- Add ScreenPlayUtil to dev docs
