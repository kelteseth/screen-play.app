---
title: "ScreenPlay v0.15 RC-4 Released!"
date: 2023-01-08T12:00:00+02:00
image: "images/blog/update_v015-rc4.png"
description: "ScreenPlay v0.15 RC-4 Released!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

Happy New Year Everyone! We have some exiting new features and fixes [since RC-1](https://screen-play.app/blog/screenplay_v0.15-rc-1/)! 🎉🎉

### Changes since last RC
- None public RC-3 and RC-4 for choco package testing. The current RC-4 only works via Steam and direct download for now.
- Window handling
  - Revert custom window handling changes from RC-1 and add Qt dark window navigation mode support. This is less buggy and also does not remove all window shortcuts, behaviour and shadows.
  - Add explicit quit and minimize warning.
- Windows
  - Fix windows wallpaper fade in calculation. We know correctly calculate the default Windows wallpaper position for nice fade-in animations.
- MacOS
  - Revert to 6.3.2 until WebEngine issues are resolved. See [my bugreport here](https://bugreports.qt.io/browse/QTBUG-96406).
  - Fix Wallpaper showing app icon in dock.
- Wizards
  - Add qml project file to QML Wallpaper. This will lets user easier edit QML Wallpaper via QtCreator
- Widgets
  - Small icon and window handling fixes.
- Development
  - Deprecate QtMaintanance based Qt setup. We now use aqt directly and thus no longer require a Qt account. If you are using VSCode you can simply install the [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) and [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) with the newly added `CMakePresets.json`. QtCreator still has some issues, see [my bugreport here](https://bugreports.qt.io/browse/QTCREATORBUG-28606).
- Linux
  - Add working linux CI. This also includes a working KDE Wallpaper version that is not yet 100% ready.

### How get ScreenPlay v0.15 RC 2 via Steam
The easiest way is to change  ScreenPlay -> Settings -> Betas -> Nightly. Standalone installer is Windows only for now.

<br>
<p class="text-center">
  <img src="/images/blog/nightly.png" />
</p> 


### Direct download (Windows x64)

<br>

<div class="text-center mt-5">
  <a  href="https://kelteseth.com/releases/0.15.0-RC4/ScreenPlay-Installer.exe">
    <button type="button" class="btn btn-success"><span class="ti-download"></span> ScreenPlay-Installer.exe</button>
  </a>
  <a  href="https://kelteseth.com/releases/0.15.0-RC4/ScreenPlay-Installer.zip">
    <button type="button" class="btn btn-secondary"><span class="ti-download"></span> ScreenPlay-Installer.zip</button>
  </a>
  <a  href="https://kelteseth.com/releases/0.15.0-RC4/ScreenPlay-0.15.0-RC4-x64-windows-release.zip">
    <button type="button" class="btn btn-secondary"><span class="ti-download"></span> Portable ScreenPlay zip</button>
  </a>
</div>
<div class="text-center mt-1">
<a href="https://kelteseth.com/releases/0.15.0-RC4/ScreenPlay-0.15.0-RC4-x64-windows-release.zip.sha256.txt">Portable ScreenPlay zip sha256 checksum</a>
</div>
<div class="text-center mt-5">
  <a  href="https://forum.screen-play.app/t/screenplay-v0-15-0-rc-4/143">
      <button type="button" class="btn btn-primary " >
        <span class="ti-face-smile"></span>
        Join the pre-release discussion
      </button>
  </a>
</div>