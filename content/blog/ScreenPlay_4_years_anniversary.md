---
title: "Celebrating 4 Years Of ScreenPlay  🎉"
date: 2021-03-25T12:00:00+02:00
image: "images/blog/4_years.png"
description: "ScreenPlay Is Now 4 Years Old!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

Today the *25.03.2021* marks the 4 year anniversary of the [first ever ScreenPlay commit](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b83116ff4f2f87e3e92e813eba6bded2fee9b894). Since then ScreenPlay has grown with the help of 10 contributors to more than 1.700 commits, 20k+ lines of QML/C++ code, 13 releases, thousands of daily active Steam users and a [growing collection of content!](https://steamcommunity.com/app/672870/workshop/) In the last 4 years ScreenPlay has seen a numerous amount of iterative changes like the move from QMake to CMake, from a single executable to multiple for better crash protections, multilingual support, wizards for wallpaper & widget and so much more!

<br>

### Looking back
Having bought Wallpaper Engine but being disappointed with its usability I started to search for Open Source alternates. The idea was to create a prototype to fill this gap and release it in the summer of the same year (2017). This release never happened. It took about 2.5 years for the Steam closed alpha testing and then more than a additional year for the public alpha to be released.

<br>

#### The First Prototype
You can browse the first ever commit [here](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b83116ff4f2f87e3e92e813eba6bded2fee9b894) (as [files](https://gitlab.com/kelteseth/ScreenPlay/-/tree/b83116ff4f2f87e3e92e813eba6bded2fee9b894)). It roughly contains a project file, a ScreenPlay class and QML that was used back then for the actual wallpaper window. This prototype was made possible by the help of Patrick ([ReShade](https://reshade.me/)) ♥.

Every platform needs a different technique when trying to display a window beneath the regular desktop icons. On Windows we have a separate window for the desktop image and the desktop icons. For this we simply iterate through all windows to search for a window called "WorkerW". This is as big as all your monitors combined. Now we only have to calculate the selected monitor offset for this and we are good to go. 

<br>

### The Future
#### Platform Support
I'm currently working on getting [macOS support up and running](https://gitlab.com/kelteseth/ScreenPlay/-/merge_requests/56). We already have a working prototype, but I personally have very little experience with macOS so this will take a bit more time to be finished. MacOS has a similar mechanism like Windows for drawing icons into a separate window. ScreenPlay already works really well on MacOS. This code was originally written by [Dominik](https://twitter.com/_Graphicscore_) ♥. If anyone wants to help with the MacOS port please contact me via Discord/Gitter.

The next platform I want to target is KDE. Here we already have QML desktop support. For this we do not need to create a  window hack like we do on Windows and MacOS. We do need to write a completely separate logic on how to display and communicate. This still needs to handle all edge cases like multi monitor support etc. Like with the MacOS version if anyone wants to help with this please contact me via Discord/Gitter.


<br>

#### Widgets & Sysinfo
There is a bare bone implementation for widgets in ScreenPlay. You can already create widgets in QML or HTML5 that displays stats like CPU and drive usage on Windows, but the plan is to go even further with better interactions and features. Because ScreenPlay tries to be as cross platform as possible we need to implement all of these for Linux and MacOS as well. 


<br>

### Thank You!
To all people who have helped and supported this project 🎉

<br>

----

<br>
