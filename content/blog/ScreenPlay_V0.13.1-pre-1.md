---
title: "ScreenPlay v0.13.1-pre 1 released! Many bugfixes, polishing, Installed sorting, Steam Tags searches and paging support."
date: 2021-03-14T17:30:00+02:00
image: "images/blog/update_v0131-preview1.png"
description: "ScreenPlay v0.13.1-pre 1 released! Many bugfixes, polishing, Steam Tags searches and paging support."
author: "Elias Steurer | Kelteseth"
type: "post"
---

This is a Preview release. [You can enable this in Steam via the nightly channel](https://kelteseth.gitlab.io/ScreenPlayDocs/faq/faq/#how-can-i-enable-steam-nightly-builds). This is not intended for general use! Please test this and provide feedback ♥!

<br>

### Website
- New Blog Post: [Learn the basics of QML for Wallpapers and Widgets in 5 minutes](https://screen-play.app/blog/guide_learn_the_basics_of_qml/)
- Cleanup old blog posts and add custom image.
- [Many articles have been updated in our wiki!](https://kelteseth.gitlab.io/ScreenPlayDocs/)

<br>

### V0.13.1 Preview 1 Patchnotes:
##### General 
- Fixed: The second wallpaper does not close properly [issue 123](https://gitlab.com/kelteseth/ScreenPlay/-/issues/123)
- Add logging to sentry This will only get send on crash to help to understand the context of the crash
##### Community
- Add newsfeed from https://screen-play.app/blog/

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/ffe834f4c820ebdac3ae96fe578b7bc4/image.png)
##### Workshop
- Refactor search to be selectable and add icons

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/e747b69475cb0ac37f2f4f44c222e78d/image.png)
- Add paging support

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/d54a9690141214a3ff6a1548e157cfcb/image.png)
- Add mouse WaitCursor indicator on load
- Add tag and tag search

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/85b36c7f7267c37372e7df90271cac46/image.png)

- Fix background blur animation and values
- Refactor sidebar to better include tags. These tags are now clickable to be used for quick search!
- Refactor Steam plugin to unload when switching back to and other page like "Installed"
- Replace laggy navigation snapping with fixed
- Fix preview image missing if workshop item has no gif preview
- Replace custom TextField search with regular TextField
##### Wallpaper
- Fix set replace wallpaper from type video to gif
- Fix settings wallpaper value
##### Installed
- Add sort by date for installed content
- Fix invisible Navigation height blocking imput
##### Language
- Add Portuguese (Brazil) translation. Thanks to Douglas Vianna @dgsmiley18 🐱‍🏍
##### Sysinfo
- Add uptime to sysinfo. You now can use the Sysinfo QML plugin to display how long 
##### MacOS
- Most work is currently happening here: https://gitlab.com/kelteseth/ScreenPlay/-/merge_requests/56
- Fix macos compilation
- Fix macos dynlib
##### Code cleanup
- Move used code into own ScreenPlayUtil library to be used elsewhere like project parsing and content type to/from string
- Move projectfile to be used elsewhere
##### Docs
- Add ScreenPlayUtil to dev docs
