---
title: "ScreenPlay v0.13.2 Released! Small Video Import Fixes!"
date: 2021-05-08T7:30:00+02:00
image: "images/blog/update_v0132.png"
description: "ScreenPlay v0.13.2 Released! Small Video Import Fixes!"
author: "Elias Steurer | Kelteseth"
type: "post"
---

### Website
New blog post
- [ScreenPlay Video Wallpaper Guide: How To Create Video Wallpaper From Your Favorite Game](/blog/guide_create_seamless_video_wallpaper/)

### Updates
This is only a small bufix mostly about video imports:

- Fix import video quality and performance
- Remove never really used benchmark and ganalytics
- Fix missing toLocal function to fix drag and drop
- Reverse slider to indicate that smaller values are better