---
title: "🎉 ScreenPlay v0.12 released! Steam Workshop And Community Page Overhaul, Instant Wallpaper Replacement & Profile Savings! "
date: 2020-09-04T18:00:00+02:00
image: "images/blog/update_v012.png"
description: "🎉 ScreenPlay v0.12 released! Steam Workshop And Community Page Overhaul, Instant Wallpaper Replacement & Profile Savings! "
author: "Elias Steurer | Kelteseth"
type: "post"
---



### Major Changes And Features
#### 🎉 Steam workshop overhaul: Search, Sorting, better Upload and many fixes!

{{< youtube E12mxEd-hk0  >}}
#### 🎉 Community page revamp! Now contains: Wiki, Fourm, Issues,  Release Notes and Contributing guide!

{{< youtube eOeGyIW-ftk  >}}

#### 🎉 Instant Wallpaper replacement
https://twitter.com/Kelteseth/status/1274708208704241667

{{< youtube PqDHmvHSsVc  >}}
#### 🎉 Profile savings. ScreenPlay now saves all settings like volume, custom properties and Widgets position!
https://twitter.com/Kelteseth/status/1284520320100171777

![1598203532373-2f7d66a6-ce0f-4a1c-8b34-373b0873b777-grafik](https://gitlab.com/kelteseth/ScreenPlay/uploads/41217a3f56b6ab0b8cdeee628993d156/1598203532373-2f7d66a6-ce0f-4a1c-8b34-373b0873b777-grafik.png)

#### 🎉 Quick deletion of local content (Not possible for workshop content for now)

![1598204113156-unknown](https://gitlab.com/kelteseth/ScreenPlay/uploads/74549e180ef9fd0cf18013bcd53646bb/1598204113156-unknown.png)

### Misc
* [e1b40a7](https://gitlab.com/kelteseth/ScreenPlay/-/commit/e1b40a7) More settings save reworking
* [0d7cb55](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0d7cb55) Move more functions into dedicated ScreenPlayWallpaper class
* [3c1f5f7](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3c1f5f7) Replace setContextProperty that will be removed in Qt6 with qmlRegisterSingletonInstance
* [5f0d81d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5f0d81d) Change sidebar monitor selection to no longer clipAdd flickable when the selection area is bigger than the sidebar
* [cb6b438](https://gitlab.com/kelteseth/ScreenPlay/-/commit/cb6b438) Change sdkconnector to use GlobalVariables::getAvailableTypes
* [5fb1ea3](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5fb1ea3) Change installed content type from string to enumWe now use the InstalledType enum in QML for differentiating between wallpaper types
* [b13d643](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b13d643) Change cmake to compile into a separate bin folder for easier deployment
* [c289a3e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c289a3e) Change monitor preview size when only a single monitor is in use
* [f7a7844](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f7a7844) Change compilation mode to c++ 20Fix qml compilation in release mode
* [a83d1fc](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a83d1fc) Change wallpaper selection to always select the first monitor
* [0b84c37](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0b84c37) Change SaveNotification base item to custom implemetation
* [c4423a1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c4423a1) Refactor saving/loading of profilesWe now partially support saving and loading custom properties.
    *  This is accomplished via a flat ProjectSettingsListModel that is only used for the properties. We still
display the DefaultVideoControls.qml if the content is a video. For this I added
the m_installedType to the MonitorListModel.


* [49b75a4](https://gitlab.com/kelteseth/ScreenPlay/-/commit/49b75a4) Move SDKConnectors SDKConnection class into seperate fileThe next will be to merge SDKConnector into the ScreenPlayManager.

    * We now have the first version of bi directional messages. This is for updating values like
saving the current Widget position. We also save a SDKConnection shared reference inside
our ScreenPlayWallpaper or ScreenPlayWidget instance. So we now have all logic in these classes.
* [14991cb](https://gitlab.com/kelteseth/ScreenPlay/-/commit/14991cb) Change Resources into two seperate qrc files

    * Also we now use qmlRegisterUncreatableMetaObject with a namespace for every enum to "emulate" enum classes in QML


* [b81c513](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b81c513) Revert setWallpaperValue from QVaraint to QStringThis caused some floating convertion error. Because they get send as a string in the end anyways we just can use string here.

* [b08c124](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b08c124) Change check if steam version from qml to cpp 
We now have a steamVersion property to set
if we can load the steam plugin. Because of this
we no longer need the WorkshopLoader and WorkshopAvailableTest
being triggered when hovering over sidebar elements


### Language 
* [7015267](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7015267) Lil language update, also lil update for me - MagVI has become Overseen :p

### Fixes
* [9e95b40](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9e95b40) Refactor install list QML & C++
Move right click popup out of every ScreenPlayItem. This results now in a huge performance improvement
* [fa6b734](https://gitlab.com/kelteseth/ScreenPlay/-/commit/fa6b734) Fix set wallpaper volume, position and playback speed
* [1fe1fb2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1fe1fb2) Fix replacing wallpaper from different types
* [d7411d1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/d7411d1) Fix artifact path to contain only bin
* [edee509](https://gitlab.com/kelteseth/ScreenPlay/-/commit/edee509) Fix aQt paths
* [0b31d2b](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0b31d2b) Fix windows c++20 compiler error by updating Qt to 5.14.2
* [13b7bd8](https://gitlab.com/kelteseth/ScreenPlay/-/commit/13b7bd8) Fix build info compile definition
* [d930f0d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/d930f0d) Fix ffmpeg color dark styling issues
* [a37fab0](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a37fab0) Fix #99
* [4f8a4a2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4f8a4a2) Fix issue #100 by replaceing custom popup with Qt Controls popup
* [0b2a66f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0b2a66f) Fix NaN value when starting wallpaper other than video
* [8bed32a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/8bed32a) Fix workshop type not set resulting in not showing anything
* [12ab2a2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/12ab2a2) Fix SDK to properly export header files
* [88fb8d8](https://gitlab.com/kelteseth/ScreenPlay/-/commit/88fb8d8) Fix spelling
* [dd9cf8c](https://gitlab.com/kelteseth/ScreenPlay/-/commit/dd9cf8c) Fix copying of korean font
* [db637a4](https://gitlab.com/kelteseth/ScreenPlay/-/commit/db637a4) Fix cmake resources variables
* [cffeec4](https://gitlab.com/kelteseth/ScreenPlay/-/commit/cffeec4) Fix sidebar FillMode
* [f63284e](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f63284e) Fixed to fast reloading if folder content has changed
* [6d4afb5](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6d4afb5) Fix window flash on startup
* [7b4a5cb](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7b4a5cb) Fix btn being click able if it is a widget
* [9110725](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9110725) Fix german translation
* [c77bdcc](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c77bdcc) Fix ffmpeg dark theme colors
* [f98e87d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f98e87d) Fix dark theme colors for video wizardAdd some mkv/webm checks because we do  not support them yet
* [b1362ca](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b1362ca) Fix close icon color to use  Material.iconColor
* [6466906](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6466906) Fix spelling thanks to Neiffer
* [2e670ed](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2e670ed) Fix colors
* [0d1e210](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0d1e210) Fix CommunityNavItem clickFix Nav shadow
* [95c1170](https://gitlab.com/kelteseth/ScreenPlay/-/commit/95c1170) Fix dark and light background colors
* [f17b26f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f17b26f) Fix codoc combobox width and text
* [59a0204](https://gitlab.com/kelteseth/ScreenPlay/-/commit/59a0204) Fix include
* [b05ad7f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b05ad7f) Fix DefaultVideoControls initial value from ScreenPlayWallpaper
* [5d8f7c4](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5d8f7c4) Fix crash when requesting none existing profileAdd SaveNotification to indicate that the profile has been saved. 



### Additions
* [045cefa](https://gitlab.com/kelteseth/ScreenPlay/-/commit/045cefa) Add deletion of content if it is not a steam itemDeleting a steam subscription folder would be pointless
because Steam would just download it again. For now
this works only with none steam items, based if
their have a steamID in their project.json
* [3ac4b5b](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3ac4b5b) Add CI status
* [281c3cf](https://gitlab.com/kelteseth/ScreenPlay/-/commit/281c3cf) Add missing fadeIn when using QML/HTML Wallpaper
* [7df52a1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7df52a1) Add Wallpaper and Widgets settings background
* [7c0b574](https://gitlab.com/kelteseth/ScreenPlay/-/commit/7c0b574) Add basic saving and loading of Widgets
* [19636ed](https://gitlab.com/kelteseth/ScreenPlay/-/commit/19636ed) Add error message
* [1ee39de](https://gitlab.com/kelteseth/ScreenPlay/-/commit/1ee39de) Add Collaboration Guidelines
* [73730ea](https://gitlab.com/kelteseth/ScreenPlay/-/commit/73730ea) Add replace wallpaper that reuses the existing ScreenPlayWallpaper process
* [4f2854b](https://gitlab.com/kelteseth/ScreenPlay/-/commit/4f2854b) Add search type enumChange FillMode namespace
* [a72c7ed](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a72c7ed) Add qt quick compiler and fix windows cmd opening in release
* [e0e3786](https://gitlab.com/kelteseth/ScreenPlay/-/commit/e0e3786) Add cmake automatic translation generation
* [db0a775](https://gitlab.com/kelteseth/ScreenPlay/-/commit/db0a775) Add hover stealing to SidebarThis is to fix the hover event of installed content
* [c32ea21](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c32ea21) Add html files to qrcApparently this works now with newer Qt versions...
* [3399520](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3399520) Add workshop as QPlugin
* [5fcc4be](https://gitlab.com/kelteseth/ScreenPlay/-/commit/5fcc4be) Add hover open in browserAdd disabled steam
* [35e7761](https://gitlab.com/kelteseth/ScreenPlay/-/commit/35e7761) Add ping  alive to check if widget or wallpaper is still running
* [0ca674a](https://gitlab.com/kelteseth/ScreenPlay/-/commit/0ca674a) Add another check if main application is still runningThe timeout of pipes is quite high so we need to ping them regularly
* [41a4b3d](https://gitlab.com/kelteseth/ScreenPlay/-/commit/41a4b3d) Add missing rc from last commit
* [a5b84b2](https://gitlab.com/kelteseth/ScreenPlay/-/commit/a5b84b2) Add windows app icon

### Removals
* [b91b5ae](https://gitlab.com/kelteseth/ScreenPlay/-/commit/b91b5ae) Remove nlohmann include
* [3574d21](https://gitlab.com/kelteseth/ScreenPlay/-/commit/3574d21) Remove more nlohmann_json
* [49747b5](https://gitlab.com/kelteseth/ScreenPlay/-/commit/49747b5) Remove no longer used nlohman-json
* [6913934](https://gitlab.com/kelteseth/ScreenPlay/-/commit/6913934) Remove disabled threejs button
* [c76bebb](https://gitlab.com/kelteseth/ScreenPlay/-/commit/c76bebb) Remove install step
* [2b1ba81](https://gitlab.com/kelteseth/ScreenPlay/-/commit/2b1ba81) Remove WallpaperType and WidgetType to merge into InstalledTypeThis is needed because we have one type enum inside the projectfile struct.
* [cfc89e6](https://gitlab.com/kelteseth/ScreenPlay/-/commit/cfc89e6) Remove old Workshoploader
* [8838f4f](https://gitlab.com/kelteseth/ScreenPlay/-/commit/8838f4f) Remove copying of html fileIt is now in the qrc
* [f93c6a1](https://gitlab.com/kelteseth/ScreenPlay/-/commit/f93c6a1) Remove webm for now because import support is missing
* [76083ba](https://gitlab.com/kelteseth/ScreenPlay/-/commit/76083ba) Remove unnecessary states
* [81a6231](https://gitlab.com/kelteseth/ScreenPlay/-/commit/81a6231) Remove stomtReplacing it with website, forum, bugtracker etc

### Update
* [eeec1b6](https://gitlab.com/kelteseth/ScreenPlay/-/commit/eeec1b6) Update qdocs config to 5.15.1
* [9852b09](https://gitlab.com/kelteseth/ScreenPlay/-/commit/9852b09) Update ffmpeg to 4.3.1

#### Update to Qt 5.15.1
*   Wallpaper are now using the newer Chromium 80
