---
title: "🎉 ScreenPlay v0.4 released!"
date: 2019-09-18T18:00:00+02:00
image: "images/blog/update_v04.png"
description: "🎉 ScreenPlay v0.4 released!"
author: "Elias Steurer | Kelteseth"
type: "post"
---


### ScreenPlay
*  eb2ca12 - Add closing of a single wallpaper
*  9975149 - Add explicit AA_ShareOpenGLContexts because of warnings in qt 5.13.1´
*  17ba5e7 - Add workaround for the sub navigation for now. This still needs some work for other types of content.
*  18cf6be - Add git build hash to display in the settings about tab
*  594eca5 - Add file system watcher. Now checks for changed content in the installed folder.
*  17729a5 - Add initial wallpaper start on startup
*  da63c09 - Add base multi monitor wallpaper setup
*  e73471a - Add installed count to nav item

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/00edbe2b60135e199b719573f887bd8c/image.png)

*  b9bf046 - Add installed counter to properly check if content is available. This is because both qml and installedlistcontent gets loaded async at the same time
*  819ba92 - Add ratio based transition for windows 10 > 1903 for some ratios..
*  7218ef2 - Add Show the preview gif if available
*  cbc4243 - Fix displaying of the current wallpaper in the monitorlistmodelFix auto selection when a wallpaper was selected once. So when you reopen the sidebar the first monitor is no longer preselected.
*  9073bf6 - Fix refactoring of the monitor list model backend with the help of  -blazar740112
*  760b469 - Fix globalvariable usage in qml
*  42dd81c - Fix parsing of fillmode to all lowercase before comparing to the available onces
*  471ccf3 - Fix overwriting of image preview if we unset the value
*  ce3d207 - Change QGuiApplication and QQuickWindow function to the static version
*  56ef99e - Move the most important variables from settings to globalvariables
    *  QUrl m_localStoragePath;
    *  QUrl m_localSettingsPath;
    *  QUrl m_wallpaperExecutablePath;
    *  QUrl m_widgetExecutablePath;
    *  QVersionNumber m_version {1,0,0};
    *  Are needed pretty much everywhere. So with the removing we can now fix some recursive include problem with the screenplaywallpaper and screenplaywidget classes. Also some shared pointer from other classes where removed because they were no longer needed.
*  4a78968 - Change settings to use Util::getVersionNumberFromString to have less duplicate code in the futureRemove empty wallpaper config functions. See ScreenPlayManager for this.
*  3c60f0c - C++ Rename qmlutilities to global util class.
    *  Move generation of random string to util
*  Add debug log in release

![image](https://gitlab.com/kelteseth/ScreenPlay/uploads/06e38eaceed93f6a595c40bb5c84193a/image.png)

*  2d6ca4b - Replace wallpaper saving with QVector
*  cddd7a0 - Cleanup monitor reloading when configuration changed

### UI
*  74e05ff - Add state transition between different wallpaper type settingsFix setting wallpaper values
*  1828b74 - Add browser like refresh via F5
*  5f47750 - Add FillMode in the sidebar, active monitor config and windows wallpaper. This is done via HTML object-fit. See: https://developer.mozilla.org/en-US/docs/Web/CSS/object-fitChange sidebar layout to fix some regressions regarding sidebar type (wallpaper/scene/widget)
*  31f00e3 - Remove comment because we can now track events in release mode
    *  Change QML Material Style to dense for smaller desktop controls
    *  Change sidebar content title into the image
    *  Change create wallpaper button to only be active when at least one monitor is selected

### Misc
*  1db6a2f - Add OpenSSL now that it is distributed via the Qt Maintaince tools
*  0da9cc7 - Cleanup sysinfo SDK to use smart pointer

### SDK
*  9727ced - Fix closing of other types of Wallpaper.
*  272a86a - Add connection type to sdkconnector. This means we no can distinguish between wallpaper and widgets.Add remove all wallpaper button. No we can close all wallpapers and widgets seperate.
    *  Add static function to write an QJsonObject into a file. Truncate is optional.
*  3e2f2e9 - Change redirectMessageOutputToMainWindow to a member function

### Wallpaper
*  6f38c82 - Fix copy and paste error when testing the ScreenPlayWallpaper standalone
*  0d20c0d - Add wallpaper for multiple monitors. This new method is used when we have more than one wallpaper  but not when the user selected all monitors
*  e26e38e - Disable the windows mouse capture for now because it is laggy and unusable
*  b211a3e - Remove the no longer used 4th. argument (rendering backend)

