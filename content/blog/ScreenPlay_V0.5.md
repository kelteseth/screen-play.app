---
title: "🎉 ScreenPlay v0.5 released! Refactor: Make ScreenPlay Qt6 ready! "
date: 2019-10-08T18:00:00+02:00
image: "images/blog/update_v05.png"
description: "🎉 ScreenPlay v0.5 released! Refactor: Make ScreenPlay Qt6 ready! "
author: "Elias Steurer | Kelteseth"
type: "post"
---

### ScreenPlay
*  a4287d2	Cleanup and add nullptr defaults
*  de3d3f5	Remove wallpaper when monitor setup changed. For now we do not try to recreate the setup.
*  f23ab00	Add save to profile. This will probaply change in the future because we need to recreate the qjson object. Qt Json implementation does not allow for altering objects.
   *  This will be replace by [JSON for Modern C++ by nlohmann](https://github.com/nlohmann/json)
*  5598bdf	Update preview video
   *  [My merge request](https://gitlab.com/gitlab-org/gitlab/merge_requests/17444) was merged and deployed for displaying videos (not gifs!) at the same with as the *.md file instead of hardcoded 400px!
      Now we have a video with way better resolution and framerate instead of an ugly and large gif.
*  1ad4366	Move everything out of the main class for better unit tests
*  8e09ee2	Replace shared_ptr with unique_ptr where reasonable and add comments

#### Make ScreenPlay Qt6 ready:
*  ef623a8	Remove contextProperties
*  77a48d1	Fix crash on exit which was caused by trying to delete the QQmlApplicationEngine. I still unsure about the reason of the crash...
   *  Was caught off guard here after removing the contextProperties that there will be a [replacement for this in Qt 5.14 (Added qmlRegisterSingletonInstance function.)](https://wiki.qt.io/New_Features_in_Qt_5.14). ¯\\_(ツ)_/¯ 

