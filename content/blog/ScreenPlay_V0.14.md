---
title: "ScreenPlay v0.14 released! macOS support is now available! "
date: 2021-10-08T19:00:00+02:00
image: "images/blog/update_v014.png"
description: "ScreenPlay v0.14 released!  macOS "
author: "Elias Steurer | Kelteseth"
type: "post"
---

### macOS support
This release of ScreenPlay now works on macOS. This is an **early alpha** version so expect things to break, but the basic functionally should work. Be aware this is based on Qt 5.15.2 so it does not yet include native Apple Silicone support. A  Qt 6.2 version of ScreenPlay with native m1 support is [already on its way](https://gitlab.com/kelteseth/ScreenPlay/-/tree/qt6-support). 

<div style="margin-top: 50px;">
  <video  autoplay muted class="container p-0 shadow-lg" controls="true" loop="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="/videos/blog/macOS.mp4" type="video/mp4">
  </video>
  <p class="text-center"><i>This video was taken from an macmini (2018) and rendered into a iMac mockup for illustration purposes.</i> </p>
  <br>
</div>

Getting there wasn't easy. The build script have been rewritten in python to work on all platforms, platform dependent behavior like autostart, translations, packaging and signing had to be build from the ground up. All in all this took about 6 months to implement. Please report any issue you encounter [here](https://forum.screen-play.app/category/7/troubleshooting).

### Steam-less builds
From the beginning of the Steam integration ScreenPlay was designed to be shipped easily as a fully FOSS version, without any proprietary code. Steam was meant for fast installations and updates. Many of you voiced the need for a version of ScreenPlay without Steam. ScreenPlay continuos integration now builds a Steam and none Steam version. This is only the first step into bringing ScreenPlay to more platforms like [brew](https://brew.sh/) or [chocolatey](https://chocolatey.org/).
![crowdin](/images/blog/updates/ci_builds.png)

### Enable Crowdin for easy web based translations
Our gitlab master branch now gets automatically synced  with crowdin, that provided us with a open source sponsorship to use their product for free! A big you to all of our community members that helped with the translations:

<div style="margin-top: 50px;">
  <video autoplay muted class="container p-0 shadow-lg" controls="true" loop="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="/videos/blog/translations.mp4" type="video/mp4">
  </video>
  <p class="text-center"><i>The language selection dropdown is getting crowded, isn't it?</i> </p>
</div>


- MDo94
- Robert S. (ThatGuy5275)
- Eric Santos (ericsantosbr2)
- Berat Tayşı (wulcato)
- Denis Numerodeux (numerodeux)
- Asion (kmita2002)
- Simp Man (Simp)
- jh KIm (kkhmn1)
- Marc-Antoine Mouttet (TheGeeKing)
- PoorPockets McNewHold (IfiwFR)
- tretrauit
- SnowSteve

You can help translate ScreenPlay [here](https://crowdin.com/project/screenplay). Please contact us if you want to help with a language that is not listed there. 
![crowdin](/images/blog/updates/crowdin.png)

### Highlight new content
Thanks to [Debash Bora](https://twitter.com/debash_bora) for adding this neat little feature of highlighting new Wallpaper and Widgets!
<div style="margin-top: 50px;">
  <video autoplay muted class="container p-0 shadow-lg" controls="true" loop="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="/videos/blog/banner_new.mp4" type="video/mp4">
  </video>
  <p class="text-center"><i>It's the little things...</i> </p>
</div>


### Add GPU detection to SysInfo QML module
Now you can display the name of your GPU. For future releases a more detailed performance display is planned.
<div style="margin-top: 50px;">
  <video autoplay muted class="container p-0 shadow-lg" controls="true" loop="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="/videos/blog/sysinfo.mp4" type="video/mp4">
  </video>
  <p class="text-center"><i>A simple QML Wallpaper that displays system informations.</i> </p>
</div>

### Fixes
- Fix video import quality
- Fix steam upload not listing Wallpaper
- Fix video import thread leak
- Fix macOS version empty installation folder  
- ... and dozens of smaller fixes!

### Roadmap

You can have a look at all upcoming V0.15 features here https://gitlab.com/kelteseth/ScreenPlay/-/milestones/6. Most notably:
- h264/mp4 support
- KDE linux support
- Qt6 update and drop Qt5 support
- Apple m1 support
- Chocolatey support