---
title: "Imprint"
description: "This is meta description."
---

Angaben gemäß § 5 TMG:
Elias Steurer Tachiom GbR
Trautenmühleweg 23
88046 Friedrichshafen


Postanschrift:
Elias Steurer
Trautenmühleweg 23
88046 Friedrichshafen

Kontakt:
info@screen-play.app


Haftung und Urheberrecht

Alle Inhalte und Grafiken dieser Webseite sind urheberrechtlich geschützt. Die Vervielfältigung auch von Teilen des Inhaltes, insbesondere die Verwendung von Bildern, Logog, Marken, Texten und Textteilen ist nur mit schriftlicher Genehmigung der Elias Steurer Tachiom GbR zulässig.

Die Informationen auf dieser Website werden ständig überprüft und aktualisiert. Trotz aller Sorgfalt können sich aber Angaben zwischenzeitlich verändert haben. Eine Haftung oder Garantie für die Aktualität, Richtigkeit und Vollständigkeit der zur Verfügung gestellten Informationen kann daher nicht übernommen werden. Soweit diese Website Informationen von Dritten enthält oder Links zu Internetseiten Dritter aufweist, erfolgt dies ausschliesslich als zusätzlicher Service für die Nutzer. Für dort enthaltene Inhalte ist die Elias Steurer Tachiom GbR nicht verantwortlich und macht sich diese Inhalte nicht zu eigen.