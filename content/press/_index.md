---
title: "Press"
description: "ScreenPlat histroy and press kit"
---

## About
ScreenPlay is an innovative open-source project found on GitLab, focused on simplifying the process of creating engaging and dynamic Wallpapers and Widgets for desktop environments. The project, initiated and maintained by Elias Steurer aka. Kelteseth, is developed to support multiple platforms including Windows, macOS, and a in development Linux version for X11 and KDE.

ScreenPlay is in development since early 2017 with now 15 releases. The next release will be version 1.0 when the X11 and KDE support is merged.

#### Code
ScreenPlay is developed using the latest state of the art Qt6, Qml, C++ 20 and CMake. All Widgets and Wallpaper can also be written in QML with additional APIs provided by ScreenPlay like the SysInfo api for system staticstics like CPU usage and a weather forcast api.

#### Technology
ScreenPlay enables users to craft and customize their desktop experience with live Wallpapers and interactive Widgets using video files, qml, or HTML pages. This goes beyond static backgrounds, bringing an extra layer of interactivity and dynamism to your device's appearance. The Wallpapers and Widgets created through ScreenPlay are GPU optimized to ensure minimal resource usage, thus ensuring that your system performance is not compromised.

#### Steam Workshop
Furthermore, ScreenPlay hosts a Steam Workshop integration, which allows users to share and download Wallpapers and Widgets created by other community members. This way, users can continually update and transform their desktop experience with creative content from around the world.

#### Open Source (AGPLv3)
ScreenPlay is Licensed under the AGPLv3 (GNU Affero General Public License). Users can contribute to the project's codebase hosted on GitLab. The project also uses Crowdin, a community-driven platform, for translations, demonstrating its commitment to accessibility and global reach.

<br>
<br>

## Press Kit

<a href="/images/press/ScreenPlay_press_kit_logos_and_screenshots.zip"><h5 class="text-white paragraph-lg font-secondary">Download the complete press kit with screenshots and logos here (zip)</h5></a>


<br>
<br>

### Logos
Click the image to download:

<div class="section">
    <div class="container">
        <figure class="d-block mx-auto w-50">
            <a href="/images/press/icons/Logo_dark_text.svg" download>
                <img class="mb-3" alt="Logo_dark_text" src="/images/press/icons/Logo_dark_text.svg">
            </a>
            <figcaption>Logo dark text (svg)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-50">
            <a href="/images/press/icons/Logo_white_text.png" download>
                <img class="mb-3" alt="Logo_white_text" src="/images/press/icons/Logo_white_text.png">
            </a>
            <figcaption>Logo white text (png)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-50">
            <a href="/images/press/icons/Logo_white_text.svg" download>
                <img class="mb-3" alt="Logo_white_text" src="/images/press/icons/Logo_white_text.svg">
            </a>
            <figcaption>Logo white text (svg)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-25">
            <a href="/images/press/icons/Icon_dark.png" download>
                <img class="mb-3" alt="Icon_dark" src="/images/press/icons/Icon_dark.png">
            </a>
            <figcaption>Icon dark (png)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-25">
            <a href="/images/press/icons/Icon_dark.svg" download>
                <img class="mb-3" alt="Icon_dark" src="/images/press/icons/Icon_dark.svg">
            </a>
            <figcaption>Icon dark (svg)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-25">
            <a href="/images/press/icons/Icon_white.png" download>
                <img class="mb-3" alt="Icon_white" src="/images/press/icons/Icon_white.png">
            </a>
            <figcaption>Icon white (png)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-25">
            <a href="/images/press/icons/Icon_white.svg" download>
                <img class="mb-3" alt="Icon_white" src="/images/press/icons/Icon_white.svg">
            </a>
            <figcaption>Icon white (svg)</figcaption>
        </figure>
        <figure class="d-block mx-auto w-50">
            <a href="/images/press/icons/Logo_dark_text.png" download>
                <img class="mb-3" alt="Logo_dark_text" src="/images/press/icons/Logo_dark_text.png">
            </a>
            <figcaption>Logo dark text (png)</figcaption>
        </figure>
    </div>
</div>

### Screenshots
Click the image to download:

<div class="section">
    <div class="container">
        <div>
            <a href="/images/press/screenshots/ScreenPlay_Community.png" download>
                <img class="w-75 mb-3 d-block mx-auto" alt="ScreenPlay_Community" src="/images/press/screenshots/ScreenPlay_Community.png">
            </a>
            <p class="text-center">ScreenPlay Community</p>
        </div>
        <div>
            <a href="/images/press/screenshots/ScreenPlay_Create.png" download>
                <img class="w-75 mb-3 d-block mx-auto" alt="ScreenPlay_Create" src="/images/press/screenshots/ScreenPlay_Create.png">
            </a>
            <p class="text-center">ScreenPlay Create</p>
        </div>
        <div>
            <a href="/images/press/screenshots/ScreenPlay_Installed.png" download>
                <img class="w-75 mb-3 d-block mx-auto" alt="ScreenPlay_Installed" src="/images/press/screenshots/ScreenPlay_Installed.png">
            </a>
            <p class="text-center">ScreenPlay Installed</p>
        </div>
        <div>
            <a href="/images/press/screenshots/ScreenPlay_Settings.png" download>
                <img class="w-75 mb-3 d-block mx-auto" alt="ScreenPlay_Settings" src="/images/press/screenshots/ScreenPlay_Settings.png">
            </a>
            <p class="text-center">ScreenPlay Settings</p>
        </div>
        <div>
            <a href="/images/press/screenshots/ScreenPlay_Workshop.jpg" download>
                <img class="w-75 mb-3 d-block mx-auto" alt="ScreenPlay_Workshop" src="/images/press/screenshots/ScreenPlay_Workshop.jpg">
            </a>
            <p class="text-center">ScreenPlay Workshop</p>
        </div>
    </div>
</div>
