![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

# ScreenPlay website 

The ScreenPlay website is made with hugo. Based on Kross Creative template but:

*  Updated to Boostrap 5.x.
*  Removed google fonts and use system fonts.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://gohugo.io/getting-started/installing/) Hugo
1. Preview your project: `hugo serve`
1. Add content

Read more at Hugo's [documentation](https://gohugo.io/documentation/).

### Preview your site

If you clone or download this project to your local computer and run `hugo serve`,
your site can be accessed under[ localhost:1313](http://localhost:1313).

### Add blog posts

Go to /content/blog and create a new *.md text file.
Add this to the top:
```
---
title: " Title "
date: 2020-09-04T18:00:00+02:00
image: "images/blog/update.png"
description: "Description"
author: "Elias Steurer | Kelteseth"
type: "post"
---
Your blog text here.
```
